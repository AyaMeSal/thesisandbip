# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 22:44:09 2018

@author: Aya
"""
#http://disease-ontology.org/tutorial/
#disease-ontology.org/
#http://gohandbook.org/table_of_contents
import subprocess

def install(name):
    subprocess.call(['pip', 'install', name])

#from future.standard_library import install_aliases
#install_aliases()
from urllib.request import urlopen
import xmltodict



def get_DiseaseOntology(do_id):
    """
        This function retrieves the OBO-XML for a given disease Ontology term,
        using EMBL-EBI's QuickGO browser.
        Input: go_id - a valid Gene Ontology ID, e.g. GO:0048527.
    """
    DiseaseOnt_url = "http://www.disease-ontology.org/api/metadata/"+do_id
    print(DiseaseOnt_url)
    oboxml = urlopen(DiseaseOnt_url)
  #  print(oboxml.read())
    jsonString  = oboxml.read()

    #jsonResponse = json.loads(response.decode('utf-8'))

    import urllib.request
    import json
    import pprint

    response = urllib.request.urlopen(DiseaseOnt_url).read()
    jsonResponse = json.loads(response.decode('utf-8'))
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(jsonResponse)

    print(jsonResponse["definition"])
    print(jsonResponse["children"])
"""
    document_file = open("myfile.xml", "r") # Open a file in read-only mode
    original_doc = document_file.read() # read the file object
    document = xmltodict.parse(original_doc) # Parse the read document string

    # Check the response
    if(oboxml.getcode() == 200):
        obodict = xmltodict.parse(oboxml.read())
        return obodict
    else:
        raise ValueError("Couldn't receive OBOXML from QuickGO. Check GO ID and try again.")
"""
def testDO():
    do_id = 'DOID:4'
    res = get_DiseaseOntology(do_id)
    print(res)


if __name__ == '__main__':
    testDO()