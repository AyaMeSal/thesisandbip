# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 17:52:29 2018
https://github.com/IGS/disease-ontology/blob/master/scripts/oboparser.py
#http://disease-ontology.org/tutorial/
#disease-ontology.org/
#http://gohandbook.org/table_of_contents
@author: Aya
"""
import goatools
from goatools import obo_parser
import wget
import os
import Utils as ut

go_obo_url = 'http://purl.obolibrary.org/obo/go/go-basic.obo'
data_folder = os.getcwd() + '/data'

# Check if we have the ./data directory already
if(not os.path.isfile(data_folder)):
    # Emulate mkdir -p (no error if folder exists)
    try:
        os.mkdir(data_folder)
    except OSError as e:
        if(e.errno != 17):
            raise e
else:
    raise Exception('Data path (' + data_folder + ') exists as a file. '
                   'Please rename, remove or change the desired location of the data path.')

# Check if the file exists already
if(not os.path.isfile(data_folder+'/go-basic.obo')):
    go_obo = wget.download(go_obo_url, data_folder+'/go-basic.obo')
else:
    go_obo = data_folder+'/go-basic.obo'

print(go_obo)
go = obo_parser.GODag(go_obo)

#ontology loaded
"""
#Part A of the project
#Input : Environmental Condtion
#Output: - Associated Diseases
         - Molecular Functions
#        - Cellular components
#        - Biological Process
"""
def getBiologcalProcesses(condition):
    biologicalProcesses =[]
    return biologicalProcesses

def getCellularComponents(condition):
    cellularComponents =[]
    return cellularComponents

def getMolecularFunctions(condition):
    molecularFunctions =[]
    return molecularFunctions

def getAssociatedDiseases(condition):
    diseases = []
    return diseases

def getDataOnEnvironmentalCondition(condition):
    diseases = getAssociatedDiseases(condition)
    molecularFunctions = getMolecularFunctions(condition)
    cellularComponents = getCellularComponents(condition)
    biologicalProcesses = getBiologcalProcesses(condition)

    print( 'Informaion related to condition {}'.format(condition))
    print( 'Related Diseases {}'.format(diseases))
    print( 'Related Molecular Functions {}'.format(molecularFunctions))
    print( 'Related Cellular Components {}'.format(cellularComponents))
    print( 'Related Biological Processes {}'.format(biologicalProcesses))



"""
Measure how much the environmental condition and the disease are associated
this can be determined using several appraoches
    - How many ontology terms they share ?
    - Get a list of the ontology terms that are listed for the trees of both terms
    - p-values
    - genes that are overexpressed or underexpressed in shared pathways (Shared between who exactly)
#Part B of the project : Environmental condition - disease association
#Input : Environmental Condtion
#Output: - Molecular Functions
#        - Cellular components
#        - Biological Process
"""


