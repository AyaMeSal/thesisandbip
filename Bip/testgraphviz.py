#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 22 15:12:30 2018

@author: a
"""
from __future__ import print_function
from goatools.associations import read_ncbi_gene2go
import visualisedictionary as vd
from IPython.display import Image
import goatools
import numpy as np
import Utils as ut
from goatools.godag_plot import plot_gos, plot_results, plot_goid2goobj
#%%
def initializeGOEA(correctionMethod):
    from goatools.base import download_go_basic_obo
    # Get ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2go.gz
    from goatools.base import download_ncbi_associations
    from goatools.obo_parser import GODag
    from goatools.test_data.genes_NCBI_9606_ProteinCoding import GeneID2nt as GeneID2nt_human
    from goatools.go_enrichment import GOEnrichmentStudy
    obo_fname = download_go_basic_obo()
    gene2go = download_ncbi_associations()
    obodag = GODag("go-basic.obo")
    geneid2gos_human = read_ncbi_gene2go("gene2go", taxids=[9606])
    print("Loaded {N:,} annotated human genes".format(N=len(geneid2gos_human)))
    
    goeaobj = GOEnrichmentStudy(
            GeneID2nt_human.keys(), # List of human protein-coding genes
            geneid2gos_human, # geneid/GO associations
            obodag, # Ontologies
            propagate_counts = False,
            alpha = 0.05, # default significance cut-off
            methods = correctionMethod) # defult multipletest correction method
    return goeaobj
#%%
# Data will be stored in this variable
def loadGeneIds(genesFilePath):
    import os
    geneid2symbol = {}
    # Get xlsx filename where data is stored
    ROOT = os.path.dirname(os.getcwd()) # go up 1 level from current working directory
    din_xlsx = os.path.join(ROOT, genesFilePath)
    print(din_xlsx)
    # Read data
    if os.path.isfile(din_xlsx):  
        import xlrd
        book = xlrd.open_workbook(din_xlsx)
        pg = book.sheet_by_index(0)
        for r in range(pg.nrows):
            geneid , symbol = [pg.cell_value(r, c) for c in range(pg.ncols)]
            if geneid:
                geneid2symbol[int(geneid)] = symbol
    # 'p_' means "pvalue". 'fdr_bh' is the multipletest method we are currently using.
    geneids_study = geneid2symbol.keys()
    #print(geneids_study)
    return geneids_study

#%%
def runGSEA(goeaobj,geneids_study , outputFileName):
    goea_results_all = goeaobj.run_study(geneids_study)
    goea_results_sig = [r for r in goea_results_all if r.p_fdr_bh < 0.05]
    goeaobj.wr_xlsx(outputFileName +".xlsx", goea_results_sig)
    goeaobj.wr_txt(outputFileName + ".txt", goea_results_sig)
    plot_results(outputFileName+"_{NS}.png", goea_results_sig)
    return goea_results_all
#%%
def filterGOterms(goea_results_all,pCutoff):
    print("Filtering GOEA Results with Cuttoff P-value of %f" %pCutoff)
    goea_results_sig = [r for r in goea_results_all if r.p_fdr_bh < pCutoff]
    return goea_results_sig

#%%
def printGOESummary(goeobj,goea_results , pCuttoff ):
    goeobj.print_summary(goea_results , min_ratio = None , pval = pCuttoff)
#%%d
def getGoTermsIDs(GoResults):
    GOTerms = [r.GO for r in GoResults ]
    return GOTerms

def getGenesInGoTerms(GoResults):
    genesinGoTerms = []
    for r in GoResults:
        for gene in r.study_items:
            genesinGoTerms.append(gene)
    return genesinGoTerms

#%%
def getGOMatches(goList1, gOList2):
    matchesCount = 0
    commonList = list(set(goList1).intersection(gOList2))
    matchesCount = len(commonList)
    #search for smaller list in BiggerList

    return matchesCount,commonList
#%% Visulaize Dictionary Enntry
def visualizeDictionary (obo_dict, do_id):
    G = vd.KeysGraph(obo_dict)
    # Draw and save this.
    vd_filename = 'DiseaseontTest' + do_id + '.png'
    G.draw(vd_filename)
    
    # Draw this image here.
    Image(vd_filename)

#%%
# Get http://geneontology.org/ontology/go-basic.obo
GOEAobj = initializeGOEA(['fdr_bh'])
stressorNames = ['nanoParticle','ionizingRadiation']
diseaseNames = ['lungAdenocarcinoma','prostantCancer','pulmonaryVasculature','rheumatoidArthritisSynovium']
cutOffs = [0.01 , 0.03 , 0.05]
with open("output.txt",'w') as f:
    for pCutoff in cutOffs:
        for stressorName in stressorNames :
            studyGenes = loadGeneIds("Bip/geneData/"+stressorName+"_gene_result_trimmed.xlsx")
            StressorResults=runGSEA(GOEAobj,studyGenes,stressorName)
            StressorResults=filterGOterms(StressorResults,pCutoff)
            stressorGO = getGoTermsIDs(StressorResults) 
            significantStressorGenes =  getGenesInGoTerms(StressorResults)
            
            for diseaseName in diseaseNames:
                diseaseGenes = loadGeneIds("Bip/geneData/"+diseaseName+"_gene_result_trimmed.xlsx")
                diseaseResults = runGSEA(GOEAobj,diseaseGenes,diseaseName)
                diseaseResults=filterGOterms(diseaseResults,pCutoff)
                significantDiseaseGenes =  getGenesInGoTerms(diseaseResults)

                commonGenesCount , CommonGenes = getGOMatches(studyGenes , diseaseGenes)
                diseaseGO = getGoTermsIDs(diseaseResults) 
                commonGotermsCount , commonGoterms = getGOMatches(stressorGO, diseaseGO)
               # f.write("Common Genes between %s and %s : %d genes \n"%(stressorName,diseaseName,commonGenesCount))
                #print("Common GO termsbetween %s and %s : %d genes"%(stressorName,diseaseName,commonGotermsCount))
                #f.write("Common GO termsbetween %s and %s : %d goterms\n"%(stressorName,diseaseName,commonGotermsCount))
                numStudyGenes = len(studyGenes)
                numDiseaseGenes = len(diseaseGenes)
                numDiseaseGO = len(diseaseGO)
                #commonGenesInSignificantGoTerms = 
                numSigDiseaseGenes = len(significantDiseaseGenes)
                commonSigGenesCount , CommonSigGenes = getGOMatches(significantStressorGenes , significantDiseaseGenes)
                # cuttof p-value , stressorname studyGenesCount ,  diseaseName studyDiseaseCount , commonGenesCount , commonGotermsCount, portion of commongenes out of disease genes , portion of common go terms out of all disease go terms 
                f.write(" %f, %s,  %d , %s,  %d , %d , %d ,%f , %f,%f \n"%(pCutoff,stressorName,numStudyGenes,diseaseName, numDiseaseGenes,commonGenesCount,commonGotermsCount,commonGenesCount/numDiseaseGenes ,commonGotermsCount/numDiseaseGO ,commonSigGenesCount/numSigDiseaseGenes ))

#dO = ut.get_DiseaseOntology("DOID:3910")
#visualizeDictionary(dO,"DOID:3910")
#ut.get_Pathways("Gna14")




