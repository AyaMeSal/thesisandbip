# -*- coding: utf-8 -*-
"""
Created on Fri Apr  6 22:44:09 2018

@author: Aya
"""
#http://disease-ontology.org/tutorial/
#disease-ontology.org/
#http://gohandbook.org/table_of_contents
import subprocess
from orangecontrib.bio.kegg import Organism
from orangecontrib.bio.kegg import *



def install(name):
    subprocess.call(['pip', 'install', name])

#from future.standard_library import install_aliases
#install_aliases()
from urllib.request import urlopen
import xmltodict



def get_DiseaseOntology(do_id):
    """
        This function retrieves the OBO-XML for a given disease Ontology term,
        Input: do_id - a valid Gene Ontology ID, e.g. DOID:04.
    """
    DiseaseOnt_url = "http://www.disease-ontology.org/api/metadata/"+do_id
    import urllib.request
    import json
    import pprint

    response = urllib.request.urlopen(DiseaseOnt_url).read()
    jsonResponse = json.loads(response.decode('utf-8'))
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(jsonResponse)

    print(jsonResponse["definition"])
    print(jsonResponse["children"])
    return jsonResponse

def get_Pathways(geneList):
    organism = Organism("Homo sapiens")
    return organism.get_enriched_pathways(geneList)
def testDO():
    do_id = 'MESH:D054198'
    do_id = 'OMIM:614739'
    res = get_DiseaseOntology(do_id)
    print(res)


#if __name__ == '__main__':
    #testDO()
