# -*- coding: utf-8 -*-
"""
Created on Sat May 19 15:20:22 2018

@author: Aya
"""

import keras
from keras.models import Model
from keras.layers import Activation, Reshape ,Input, merge, Dense, Flatten, Conv1D, Conv2D , Lambda , multiply , Add
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import LeakyReLU
import keras.backend as K
import utils as ut
from time import time
from keras.callbacks import TensorBoard
import numpy as np
import math

tensorboard = TensorBoard(log_dir="logs/{}".format(time()))

"""
convLayer_x = Conv1D(filters =1 , kernel_size = 1 , activation = 'relu')(inp)
    K.eval(convLayer_x)
   # x_reshape = keras.backend.reshape(convLayer_x, (1,1,5))
    K.eval(x_reshape)
    convLayer_x = K.flatten(convLayer_x)

    attention_probs = Dense(numagentsLess1, activation='softmax', name='attention_vec')
    K.eval(attention_probs(x_reshape))
    sm = K.softmax(convLayer_x) # softmaxed probability
    K.eval(sm)
    sm.shape
    (K.transpose(sm)).shape
    #now multiply with original input
    convLayer_flt = keras.backend.expand_dims(sm, axis=-1)
    d = K.dot(K.transpose(convLayer_flt),inp  )
    mul = convLayer_flt * inp
    K.eval(mul)
    mul.shape
    #summation over all agents
    augmented = K.sum(mul , axis =1)
    K.eval(augmented)
    augmented.shape
"""
def get_VAIN_model(features, numagents,labels_shape):

    #features = 5; numagents=20;labels_shape=4
    singleInputShape = features
    pairwise_input_shape = singleInputShape * 2
    numAgentsLess1 = numagents
    nb_classes =  labels_shape
    hidden_size = 128
#    weights = layer.get_weights()
    singleInp = Input(shape=(singleInputShape,))
    print(singleInp.shape)
    x = Dense(hidden_size)(singleInp)
    print(x.shape)
    x = BatchNormalization(axis=1)(x)
    print(x.shape)
    x = Activation('relu')(x)
    print(x.shape)
    x = Dense(hidden_size, )(x)
    print(x.shape)
    x = BatchNormalization(axis=1)(x)
    print(x.shape)

   # x = single_dense2_bn
   # singleModel = Model(inputs=singleInp, outputs=x)
   # print(singleModel.summary())

    z = Input(shape = (numagents-1,pairwise_input_shape,) ,name='pairwiseInput')
    z.shape
    pairInp = Reshape(( -1,pairwise_input_shape ),name='paiwise_reshapes')(z)
   # pairInp.shape
   # pairInp = Lambda(lambda x : K.squeeze(x,axis=1) ,name='squeeze')(pairInp)
    pairInp.shape
    pairInp = Dense(hidden_size)(pairInp)
    pairInp = BatchNormalization(axis=1)(pairInp)
    pairInp = Activation('relu')(pairInp)
    pairInp = Dense(hidden_size)(pairInp)
    pairInp = BatchNormalization(axis=1)(pairInp)
    pairInp = Activation('relu')(pairInp)

    z_att_1 = Dense(1,name='outputLambda')(pairInp)
    z_att_1.shape
    z_att_1 = Reshape((-1,numAgentsLess1-1) ,name='attention_vec1')(z_att_1)
    z_att_1 = Lambda(lambda x : K.softmax(x) ,name='attention_vec') (z_att_1)
   # op = np.array(K.eval(z_att_1))
   # np.savetxt("foo.csv",op,delimiter =",")
    zmain = Dense(hidden_size)(pairInp)
    zmain=BatchNormalization(axis=1)(zmain)
    #print(zmain.shape[1])
    zmain = Reshape( (-1, numAgentsLess1-1, hidden_size) )(zmain)

    z_att_1 = Lambda(lambda x : K.expand_dims(x , axis=3) ,name='expand') (z_att_1)
#    z_att_1 = Lambda(lambda x : K.repeat_elements(x ,rep =pairwise_input_shape,  axis =1) ,name='pairwiseexpand') (z_att_1)


    zprod =  multiply([zmain,z_att_1])
    zprod.shape
    z_diff = Lambda(lambda x : K.sum(x,axis=2) ,name='sum')(zprod)
    z_diff = Lambda(lambda x : K.squeeze(x,axis=1) ,name='squeeze')(z_diff)

    x_z_concatenated = keras.layers.add([x, z_diff])
    beforeLast = Dense(nb_classes, activation='softmax')(x_z_concatenated)
    out = Dense(nb_classes)(beforeLast)

    fullModel = Model(inputs=[singleInp,z], outputs=out)
    print(fullModel.summary())
    return fullModel
def get_full_model_1Input_reg(hidden_size,features, numagents,labels_shape):

   # features = 5; numagents=20;labels_shape=4
    singleInputShape = features
    pairwise_input_shape = singleInputShape * 2
    numAgentsLess1 = numagents
    nb_classes =  labels_shape

    #numAgentsLess1 = 20 ; singleInputShape = 17 ;hidden_size =50;

    z = Input(shape = (numAgentsLess1,singleInputShape,),  name ='input')
  #  pairInp = Reshape((-1 ,pairwise_input_shape))(pairInp)
    print(z.shape)
    pairInp = Conv1D(filters = hidden_size , kernel_size = 1 , name ='FCN1')(z)
    print(pairInp.shape)
    pairInp = BatchNormalization(axis=1)(pairInp)
    print(pairInp.shape)
    pairInp = Activation('relu')(pairInp)
    print(pairInp.shape)
    pairInp = Conv1D(filters = math.ceil(hidden_size/2) , kernel_size = 1 ,  name ='FCN2')(pairInp)
    pairInp = BatchNormalization(axis=1)(pairInp)
    print(pairInp.shape)
    pairInp = Activation('relu')(pairInp)
    print(pairInp.shape)


    agentsSet1 = Conv1D(filters = 1 , kernel_size = 1, name='agentsSet1')(pairInp)   #first encoding for agents  in : (1 , num agents , huddenSize) out (1, numagents , 1)
    print(agentsSet1.shape)
    agentsSet2 = Conv1D(filters = 1 , kernel_size = 1, name='agentsSet2')(pairInp)   #secod encoding for agents  in : (1 , num agents , huddenSize) out (1, numagents , 1)
    print(agentsSet2.shape)
    agentsSet2 = Reshape((1 ,-1)) (agentsSet2) # transpose second agent in :( 1 , numagents , 1) out (1 , 1 , nuagents)
    print(agentsSet2.shape)
    attentionMatrixWIP = Add( name='broadcast_sets_together')([agentsSet1 , agentsSet2])
    print(attentionMatrixWIP.shape)
    attentionMatrixWIP = LeakyReLU(name='LeakyRelu_after_broadcast',alpha=.003)(attentionMatrixWIP)
    print(attentionMatrixWIP.shape)
    attentionMatrix = Lambda(lambda x : K.softmax(x) ,name='attention_vec') (attentionMatrixWIP)
    print(attentionMatrix.shape)
    #matrixMul = Lambda(lambda x : K.dot(x[1],K.transpose(x[0])) ,name='matMul')([pairInp , attentionMatrix])
    matrixMul = keras.layers.dot([attentionMatrix ,pairInp],1)
    print(matrixMul.shape)
    # transpose second agent in :( 1 , numagents , 1) out (1 , 1 , nuagents)
    print(matrixMul.shape)


    #startDecoding
   
    decoding = Conv1D(filters =math.ceil( hidden_size/2) , kernel_size = 1 , name ='decoder1')(matrixMul)
    decoding = BatchNormalization(axis=1)(decoding)
    decoding = Activation('relu')(decoding)
    decoding = Conv1D(filters = hidden_size , kernel_size = 1 , name ='decoder2' , activation = 'relu')(decoding)
    decoding = BatchNormalization(axis=1)(decoding)
    decoding = Activation('relu')(decoding)
    out = Conv1D(filters =nb_classes , kernel_size = 1 , name ='output')(decoding)



    fullModel = Model(inputs=[z], outputs=out)
    print(fullModel.summary())
    return fullModel


def get_full_model_1Input(hidden_size,features, numagents,labels_shape):

   # features = 5; numagents=20;labels_shape=4
    singleInputShape = features
    pairwise_input_shape = singleInputShape * 2
    numAgentsLess1 = numagents 
    nb_classes =  labels_shape
   
    #numAgentsLess1 = 20 ; singleInputShape = 17 ;hidden_size =50;
    
    z = Input(shape = (numAgentsLess1,singleInputShape,),  name ='input')
  #  pairInp = Reshape((-1 ,pairwise_input_shape))(pairInp)
    print(z.shape)
    pairInp = Conv1D(filters = hidden_size , kernel_size = 1 , name ='FCN1')(z)
    print(pairInp.shape)
    pairInp = BatchNormalization(axis=1)(pairInp)
    print(pairInp.shape)
    pairInp = Activation('relu')(pairInp)
    print(pairInp.shape)
    pairInp = Conv1D(filters = hidden_size , kernel_size = 1 ,  name ='FCN2')(pairInp)
    pairInp = BatchNormalization(axis=1)(pairInp)
    print(pairInp.shape)
    pairInp = Activation('relu')(pairInp)
    print(pairInp.shape)


    agentsSet1 = Conv1D(filters = 1 , kernel_size = 1, name='agentsSet1')(pairInp)   #first encoding for agents  in : (1 , num agents , huddenSize) out (1, numagents , 1)
    print(agentsSet1.shape)
    agentsSet2 = Conv1D(filters = 1 , kernel_size = 1, name='agentsSet2')(pairInp)   #secod encoding for agents  in : (1 , num agents , huddenSize) out (1, numagents , 1)
    print(agentsSet2.shape)
    agentsSet2 = Reshape((1 ,-1)) (agentsSet2) # transpose second agent in :( 1 , numagents , 1) out (1 , 1 , nuagents)
    print(agentsSet2.shape)
    attentionMatrixWIP = Add( name='broadcast_sets_together')([agentsSet1 , agentsSet2])
    print(attentionMatrixWIP.shape)
    attentionMatrixWIP = LeakyReLU(name='LeakyRelu_after_broadcast',alpha=.003)(attentionMatrixWIP)
    print(attentionMatrixWIP.shape)
    attentionMatrix = Lambda(lambda x : K.softmax(x) ,name='attention_vec') (attentionMatrixWIP)
    print(attentionMatrix.shape)
    #matrixMul = Lambda(lambda x : K.dot(x[1],K.transpose(x[0])) ,name='matMul')([pairInp , attentionMatrix])
    matrixMul = keras.layers.dot([attentionMatrix ,pairInp],1)
    print(matrixMul.shape)
    concat = keras.layers.concatenate([pairInp ,matrixMul], name="concatenate_Input")
    # transpose second agent in :( 1 , numagents , 1) out (1 , 1 , nuagents)
    print(matrixMul.shape)

    
    #startDecoding
    decoding = Conv1D(filters = nb_classes , kernel_size = 1 , name ='decoder' , activation = 'relu')(concat)
    out  = Lambda(lambda x : K.softmax(x) ,name='output') (decoding) #was softmax for clasification

    fullModel = Model(inputs=[z], outputs=out)
    print(fullModel.summary())
    return fullModel







def get_full_model_reg(hidden_size,features, numagents,labels_shape):

    # features = 5; numagents=20;labels_shape=4
    singleInputShape = features
    pairwise_input_shape = singleInputShape * 2
    numAgentsLess1 = numagents 
    nb_classes =  labels_shape
    #hidden_size = 128
    
    
    singleInp = Input(shape=(singleInputShape,))
    print(singleInp.shape)
    x = Dense(hidden_size,)(singleInp)
    print(x.shape)
    x = BatchNormalization(axis=1)(x)
    print(x.shape)
    x = Activation('relu')(x)
    print(x.shape)
    x = Dense(hidden_size, )(x)
    print(x.shape)
    x = BatchNormalization(axis=1)(x)
    print(x.shape)
    x = Activation('relu')(x)


   # x = single_dense2_bn
   # singleModel = Model(inputs=singleInp, outputs=x)
   # print(singleModel.summary())

    z = Input(shape = (numAgentsLess1,pairwise_input_shape,))
  #  pairInp = Reshape((-1 ,pairwise_input_shape))(pairInp)
    print(z.shape)
    pairInp = Conv1D(filters = hidden_size , kernel_size = 1 , name ='FCN1' )(z)
    print(pairInp.shape)
    pairInp = BatchNormalization(axis=1)(pairInp)
    print(pairInp.shape)
    pairInp = Activation('relu')(pairInp)
    print(pairInp.shape)
    pairInp = Conv1D(filters = hidden_size , kernel_size = 1 ,  name ='FCN2')(pairInp)
    pairInp = BatchNormalization(axis=1)(pairInp)
    print(pairInp.shape)
    pairInp = Activation('relu')(pairInp)
    print(pairInp.shape)


    attWeights = Conv1D(filters = 1 , kernel_size = 1)(pairInp)   
    attWeights = Reshape((1 ,-1))(attWeights)

    attWeights = Lambda(lambda x : K.softmax(x) ,name='attention_vec') (attWeights)
    print(attWeights.shape)
    attWeights = Reshape((-1,1))(attWeights)
    mul = multiply([pairInp, attWeights])#(batch,numagents , encodeing)
    print(mul.shape) 
    #axis 1 because we want to squesh fetures for  all the agents ( represented by axis 1 in the 3D matrix (batch, agents,encoding))
    squeshedFeaturesFromOthers = Lambda(lambda xin: K.sum(xin, axis=1))(mul)
    squeshedFeaturesFromOthers = LeakyReLU(alpha=.003)(squeshedFeaturesFromOthers)
    print(squeshedFeaturesFromOthers.shape)
    
    x_z_concatenated = keras.layers.concatenate([x, squeshedFeaturesFromOthers])
    encodeconcatunated = Dense(hidden_size)(x_z_concatenated)
    encodeconcatunated = BatchNormalization(axis=1)(encodeconcatunated)
    encodeconcatunated = Activation('relu')(encodeconcatunated)
    encodeconcatunated = Dense(hidden_size)(encodeconcatunated)
    encodeconcatunated = BatchNormalization(axis=1)(encodeconcatunated)
    encodeconcatunated = Activation('relu')(encodeconcatunated)
    out = Dense(nb_classes, activation='linear')(encodeconcatunated) #was softmax for clasification

    fullModel = Model(inputs=[singleInp,z], outputs=out)
    print(fullModel.summary())
    return fullModel

def get_full_model_class(hidden_size,features, numagents,labels_shape):

   # features = 5; numagents=20;labels_shape=4
    singleInputShape = features
    pairwise_input_shape = singleInputShape * 2
    numAgentsLess1 = numagents 
    nb_classes =  labels_shape
    #hidden_size = 128
    
    
    singleInp = Input(shape=(singleInputShape,))
    print(singleInp.shape)
    x = Dense(hidden_size,)(singleInp)
    print(x.shape)
    x = BatchNormalization(axis=1)(x)
    print(x.shape)
    x = Activation('relu')(x)
    print(x.shape)
    x = Dense(hidden_size, )(x)
    print(x.shape)
    x = BatchNormalization(axis=1)(x)
    print(x.shape)

   # x = single_dense2_bn
   # singleModel = Model(inputs=singleInp, outputs=x)
   # print(singleModel.summary())

    z = Input(shape = (numAgentsLess1,pairwise_input_shape,))
  #  pairInp = Reshape((-1 ,pairwise_input_shape))(pairInp)
    print(z.shape)
    pairInp = Conv1D(filters = hidden_size , kernel_size = 1 , name ='FCN1')(z)
    print(pairInp.shape)
    pairInp = BatchNormalization(axis=1)(pairInp)
    print(pairInp.shape)
    pairInp = Activation('relu')(pairInp)
    print(pairInp.shape)
    pairInp = Conv1D(filters = hidden_size , kernel_size = 1 ,  name ='FCN2')(pairInp)
    pairInp = BatchNormalization(axis=1)(pairInp)
    print(pairInp.shape)
    pairInp = Activation('relu')(pairInp)
    print(pairInp.shape)


    attWeights = Conv1D(filters = 1 , kernel_size = 1)(pairInp)   
    attWeights = Reshape((1 ,-1))(attWeights)

    attWeights = Lambda(lambda x : K.softmax(x) ,name='attention_vec') (attWeights)
    print(attWeights.shape)
    attWeights = Reshape((-1,1))(attWeights)
    mul = multiply([pairInp, attWeights])#(batch,numagents , encodeing)
    print(mul.shape) 
    #axis 1 because we want to squesh fetures fro all the agents ( represented by axis 1 in the 3D matrix (batch, agents,encoding))
    squeshedFeaturesFromOthers = Lambda(lambda xin: K.sum(xin, axis=1))(mul)
    squeshedFeaturesFromOthers = LeakyReLU(alpha=.001)(squeshedFeaturesFromOthers)

    print(squeshedFeaturesFromOthers.shape)
    
    x_z_concatenated = keras.layers.add([x, squeshedFeaturesFromOthers])
    encodeconcatunated = Dense(hidden_size)(x_z_concatenated)
    encodeconcatunated = BatchNormalization(axis=1)(encodeconcatunated)
    encodeconcatunated = Activation('relu')(encodeconcatunated)
    beforeLast = Dense(nb_classes, activation='relu')(encodeconcatunated) #was softmax for clasification
    out = Dense(nb_classes, activation='softmax')(beforeLast)

    fullModel = Model(inputs=[singleInp,z], outputs=out)
    print(fullModel.summary())
    return fullModel

    #K.eval(attention_probs(x_reshape))
    #TODO:remove Softmax activation and put this back ? sm =Lambda(lambda x : K.softmax(x) ,name='attention_vec') (convLayer_x)
    #print(sm.shape)
# , output_shape = (1 , pairwise_input_shape))(convLayer_x)
    #sm = Softmax(convLayer_x)# axis =-1)
   # K.eval(sm)
    #sm.shape
    #(K.transpose(sm)).shape
    #now multiply with original input
    """
    ##convLayer_flt = keras.backend.expand_dims(sm, axis=-1)
    sm = Lambda(lambda x :keras.backend.expand_dims(x, axis=-1) )(sm)
    #print(sm.output_shape)
    # d = K.dot(K.transpose(convLayer_flt),inp  )
    #mul = convLayer_flt * pairInp
    #mul= Lambda(lambda x:  x * pairInp)(sm)
    #from keras.layers import multiply
    mul = multiply([pairInp, sm])
   
    inp =augmented
    pairs_encoded = Dense(hidden_size)(inp)
    pairs_encoded = BatchNormalization(axis=1)(pairs_encoded)
    pairs_encoded = Activation('relu')(pairs_encoded)
    pairs_encoded = Dense(hidden_size)(pairs_encoded)
    pairs_encoded = BatchNormalization(axis=1)(pairs_encoded)
    pairs_encoded = Activation('relu')(pairs_encoded)
    pairs_encoded.shape
    ##


    """

    """ # ATTENTION PART STARTS HERE
    attention_probs = Dense(pairwise_input_shape, activation='softmax', name='attention_vec')(pairInp)
    attention_mul = merge([pairInp, attention_probs], output_shape=input_dim, name='attention_mul', mode='mul')
    """# ATTENTION PART FINISHES HERE
#    zWithAtt = Dense(14 , activation='sigmoid')(attention_mul)
   # output = Dense(num_classes , activation='sigmoid')(attention_mul)
    #model = Model(input=[inputs], output=output)
    #return model
    """
    x_z_concatenated = keras.layers.add([x, pairs_encoded])
    beforeLast = Dense(nb_classes, activation='softmax')(x_z_concatenated)
    out = Dense(nb_classes, activation='relu')(beforeLast)

    fullModel = Model(inputs=[singleInp,pairInp], outputs=out)
    print(fullModel.summary())
    return fullModel
    """


def get_model(single_shape, pairs_shape,labels_shape):

    rows, cols = 28 , 28
    features = 5
    singleInputShape = single_shape
    pairwise_input_shape =  pairs_shape

    nb_classes=labels_shape
    hidden_size = 128

    singleInp = Input(shape=(singleInputShape,))
    x = Dense(hidden_size, activation='relu')(singleInp)
    x = BatchNormalization(axis=1)(x)
    x = Dense(hidden_size, activation='relu')(x)
    x = BatchNormalization(axis=1)(x)
   # x = single_dense2_bn
   # singleModel = Model(inputs=singleInp, outputs=x)
   # print(singleModel.summary())

    pairInp = Input(shape = (pairwise_input_shape,))
    # ATTENTION PART STARTS HERE
    attention_probs = Dense(pairwise_input_shape, activation='softmax', name='attention_vec')(pairInp)
    attention_mul = merge([pairInp, attention_probs], output_shape=input_dim, name='attention_mul', mode='mul')
    # ATTENTION PART FINISHES HERE
    zWithAtt = Dense(14 , activation='sigmoid')(attention_mul)
   # output = Dense(num_classes , activation='sigmoid')(attention_mul)
    #model = Model(input=[inputs], output=output)
    #return model
    x_z_concatenated = keras.layers.concatenate([x , zWithAtt]) #also try add
    beforeLast = Dense(nb_classes, activation='softmax')(x_z_concatenated)
    out = Dense(nb_classes, activation='relu')(beforeLast)
    fullModel = Model(inputs=[singleInp,pairInp], outputs=out)
    print(fullModel.summary())
    return fullModel


    #constructing ModelZ
"""
    pairInp = Input(shape= pairwise_input_shape)
    pair_dense1_1_bn = BatchNormalization(axis=1)(pairInp)
    pair_dense_1_2 = Dense(hidden_size, activation='relu')(pair_dense1_1_bn)
    pair_dense1_2_bn = BatchNormalization(axis=1)(pair_dense_1_2)
    pair_dense1_3_main = Dense(hidden_size, activation='relu')(pair_dense1_2_bn)
    z = pair_dense1_3_main
  #  pair_Z_Model = Model(inputs=pairInp, outputs=z)
    x_z = keras.layers.Add()([x, z])
    beforeLast = Dense(nb_classes, activation='softmax')(x_z)
    out = Dense(nb_classes, activation='relu')(beforeLast)

    fullModel = Model(inputs=[singleInp,pairInp], outputs=out)
    print(fullModel.summary())
"""



input_dim = 32



"""
   z_att_1 = Dense(1, activation='softmax')(z)
#    z_att_1 = z_att_1.view(-1, 19)

    z_main = BatchNormalization(axis=1)(z)
#    z_main = z_main.view(-1, 19, z_main.size(1)) #any number of rows but must be 19 columns and third dimension is
    z_prod = z_main * z_att_1
    z_diff = np.sum(z_prod)#.squeeze()
"""


if __name__ == '__main__':

  #  model = get_full_model(5 ,20 , 4)
   # model = get_full_model(5 ,20 , 4)
    #model.summary()
    from IPython.display import SVG
    from keras.utils.vis_utils import model_to_dot
#    SVG(model_to_dot(model).create(prog='dot', format='svg'))

