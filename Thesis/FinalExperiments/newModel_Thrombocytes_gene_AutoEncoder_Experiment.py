import utils as ut
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
from sklearn.decomposition import PCA
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
import numpy as np
import os
#%%

numEpochs = 1
weightsFileName = "Gene_Autoencoder_new_model_best.h5"
hidden_size = 100
epochcount = 100

if __name__ =="__main__":
	geneExpFile="C:/Users/Aya/bitBucketRepo/cleanCheckout/thesisandbip/Thesis/700_genes.csv"
	geneExpFile="./Data/GE_mvg.csv"
	#annotationFile="C:/Data/Annotation.csv"
	#Annotations= read_csv(annotationFile)
	#clusterLabels = Annotations['State']
	df = read_csv(geneExpFile)
	geneExpression = np.array(df)


	scaler = MinMaxScaler()
	scaler.fit(geneExpression)
	geneExpression = scaler.transform(geneExpression)
	#%%
	geneExpInput = geneExpression.T
	#geneExpInput= np.expand_dims((geneExpInput), axis=1)
	lbls = geneExpression.T
	#lbls= np.expand_dims((geneExpOutput), axis=1)

	#X_train, X_test, y_train, y_test = ut.train_test_split(geneExpInput,lbls, 0)
	#singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data_self(X_train , y_train)
	#singleton_test , pairwise_test , labels_test ,pairwiseLabels_test = ut.get_data_self(X_test , y_test)
	singleton_train = geneExpInput
	labels_train = geneExpInput

	#geneExpInput, X_test, geneExpOutput, y_test = ut.train_test_split(geneExpInput,geneExpOutput, 0.2)
	import ThesisModel_AttentionMatrix as tm
	from keras import metrics

	#import ThesisModel as t
	import keras
	#adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)#, amsgrad=False)
	GeneAutoEncoder  = tm.get_full_model_1Input_reg(100,singleton_train.shape[1],singleton_train.shape[0],labels_train.shape[1])
	#VAIN_Evolution.compile(loss='mean_squared_error', optimizer= adam, metrics=[metrics.mae])
	GeneAutoEncoder.compile(loss='mse', optimizer= 'adadelta', metrics=[metrics.mae])
	singleton_train=np.expand_dims(singleton_train, axis =0)
	labels_train = np.expand_dims(labels_train , axis =0)

	saveBest = keras.callbacks.ModelCheckpoint(weightsFileName, monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)
	#try:
	# while True:
	GeneAutoEncoder_History = GeneAutoEncoder.fit(singleton_train, labels_train, batch_size=1 ,epochs=epochcount ,validation_data =(singleton_train , labels_train), verbose=2, callbacks =[saveBest])# , trace.attention()])
	print("Epoch number %d" %epochcount)
	epochcount= epochcount+1






	#ut.saveModel(GeneAutoEncoder , "./NewModelLogs/NewModel_GeneAutoEncoderRealData"+str(numEpochs))

	"""
	model = AttentionSeq2Seq(input_dim=geneExpInput.shape[2], input_length=geneExpInput.shape[1], hidden_dim=10, output_length=geneExpOutput.shape[1], output_dim=geneExpOutput.shape[2], depth=4)
	model.compile(loss='mse', optimizer='adam')
	history = model.fit(geneExpInput, geneExpOutput, epochs=numEpochs , verbose=2)
	testSample = np.array(geneExpInput[1]).reshape(1,1,geneExpInput.shape[2])
	prediction = geneExpOutput[1]
	print(prediction)
	model.predict(testSample)#,geneExpOutput[1])
	"""
