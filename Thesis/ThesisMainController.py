#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 15:05:32 2018

@author: a
"""
#get the data
#split up the data
#get the model
# run the model
import utils as ut
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
from sklearn.decomposition import PCA
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
import numpy as np
import attention_utils as au

#%%

#ut.fixSeeds()
#%%
numEpochs = 1000
#%% Get Data
X = ut.getClusteredData(2 , 1000)
scaler = MinMaxScaler()
scaler.fit(X)
X = scaler.transform(X)
dims = 5
"""
pca = PCA(n_components=dims)
#fit PCA model to data
pca.fit(X)
X = pca.transform(X)
"""
kmeans = KMeans(n_clusters=4)
kmeans.fit(X)
print("Cluster memberships:\n{}".format(kmeans.labels_))
#mglearn.discrete_scatter(X[:,0], X[:,1],kmeans.labels_ , markers ='o')
clstrLabels = kmeans.labels_;
labels = ut.oneHotEncoding(clstrLabels)
labels = np.array(labels)
X_train, X_test, y_train, y_test = ut.train_test_split(X,labels, 0.5)
singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data_self(X_train , y_train)
singleton_test , pairwise_test , labels_test ,pairwiseLabels_test = ut.get_data_self(X_test , y_test)

import ThesisModel_AttentionMatrix as tm
import ThesisModel as t
model_rigged = tm.get_full_model(singleton_train.shape[1],singleton_train.shape[0],labels.shape[1])
model_rigged , interactionHistory_rigged = ut.compile_fit_TwoInputModel(model_rigged, singleton_train, pairwise_train, labels_train,2,numEpochs ,'mse')
testAcc_rigged = model_rigged.evaluate([singleton_test,pairwise_test],labels_test)

attention_mat = getAttentionForSingleAgent(singleton_train[0] , pairwise_train[0],model_rigged)#cellPCA_model)
#attention_mat = attention_mat.reshape(499,499)
print(attention_mat)

ut.plotHistory(interactionHistory_rigged, "interaction")
print("complex Attention Model")
print(model_rigged.metrics_names)
print(testAcc_rigged)
#%%
# fix random seed for reproducibility
seed = 7
np.random.seed(seed)
# evaluate model with standardized dataset
estimator = KerasRegressor(build_fn=model_rigged, epochs=100, batch_size=5, verbose=0)
kfold = KFold(n_splits=10, random_state=seed)
results = cross_val_score(estimator,[singleton_train, pairwise_train], labels_train, cv=kfold)
print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))

#%%
geneExpFile="C:/Data/GE_mvg.csv"
annotationFile="C:/Data/Annotation.csv"
df = read_csv(geneExpFile)
geneExpression = np.array(df)
Annotations= read_csv(annotationFile)
clusterLabels = Annotations['State']
#%%
scaler = MinMaxScaler()
scaler.fit(geneExpression)
X = scaler.transform(geneExpression)
dims = 5
pca = PCA(n_components=dims)
#fit PCA model to data
pca.fit(X)
X = pca.transform(X)
pca2d = PCA(n_components=2)
#fit PCA model to data
pca2d.fit(X)
X2d = pca2d.transform(X)
ut.Kmeans(X2d,5)
#%%
#one-hot Encoding:
lbls = ut.oneHotEncoding(clusterLabels)
#%%
labels = np.array(lbls)

#%%Split Data
X_train, X_test, y_train, y_test = ut.train_test_split(X,labels, 0.5)
#%% 2dInput
singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data(X_train , y_train)
singleton_test , pairwise_test , labels_test ,pairwiseLabels_test = ut.get_data(X_test , y_test)
#%% Main Part :model trained on clustering labels
import ThesisModel as tm
model = tm.get_full_model(singleton_train.shape[1],singleton_train.shape[0],labels.shape[1])
model , interactionHistory = ut.compile_fit_TwoInputModel(model, singleton_train, pairwise_train, labels_train,2,numEpochs)
testAcc = model.evaluate([singleton_test,pairwise_test],labels_test)

ut.plotHistory(interactionHistory, "interaction")
print("complex Attention Model")
print(model.metrics_names)
print(testAcc)
inps = model.input[0]
#%% Model trained on cell PCA
import ThesisModel as tm
cellPCA_model = tm.get_full_model(singleton_train.shape[1],singleton_train.shape[0],singleton_train.shape[1])
cellPCA_model , cellPCA_modelHistory = ut.compile_fit_TwoInputModel(cellPCA_model, singleton_train, pairwise_train, singleton_train,2,numEpochs)
cellPCA_model_testAcc = cellPCA_model.evaluate([singleton_test,pairwise_test],singleton_test)

ut.plotHistory(cellPCA_modelHistory, "cellPCA_model")
print("cellPCA_model Attention Model")
print(cellPCA_model.metrics_names)
print(cellPCA_model_testAcc)


#%%model trained on Gene PCA
#ut.plotModel(model,'model_full__attention.png')
#%% Print attention
print (pairwise_train[0])
#attention_vector = au.get_activations(model,(singleton_train[0],pairwise_train[0]), print_shape_only=True,layer_name='attention_vec')
#[0].flatten()
print((singleton_train[0]).shape)
print((pairwise_train[0]).shape)

attention_vector = getAttentionForAgent(singleton_train[0] , pairwise_train[0] )

print('attention =', attention_vector)
#%%
# plot part.
import matplotlib.pyplot as plt
import pandas as pd

pd.DataFrame(attention_vector, columns=['attention (%)']).plot(kind='bar',
                                                               title='Attention Mechanism as '
                                                                     'a function of input'
                                                                     ' dimensions.')
plt.show()

#%%


#%%
#Goal : for every agent type Which other types get the most attention ?
# output : graph attention assigned to other classes for every given input class
#get all data points with cluster labels x
fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(8, 8))
"""
df['A'].plot(ax=axes[0,0]); axes[0,0].set_title('A')

df['B'].plot(ax=axes[0,1]); axes[0,1].set_title('B')

df['C'].plot(ax=axes[1,0]); axes[1,0].set_title('C')

df['D'].plot(ax=axes[1,1]); axes[1,1].set_title('D')
"""

for col_indx in range(0, labels_train.shape[1]):
     #make mask from clusterLabels
     hist = [0,0,0,0] #,0]
     all_col = labels_train[:,col_indx]
     cluster_mask = all_col > 0
     singletonFeaturesForClusterX = singleton_train[cluster_mask]
     pairwiseFeaturesForClusterX = pairwise_train[cluster_mask]
     LablesforOtherAgents = pairwiseLabels_train[cluster_mask]
     histForClusterX = [0,0,0,0] #,0]
     index = 0
     for classMember in singletonFeaturesForClusterX:
         singleton = classMember
         pairwise = pairwiseFeaturesForClusterX[index]
         othersLabels = LablesforOtherAgents[index]
         attention_vector = ut.getAttentionForSingleAgent(singleton , pairwise,model_rigged)#cellPCA_model)
         histForClusterX += ut.getHistForAttentionVector(attention_vector , othersLabels)
         index+=1
    #plot the graph for this agent
    #x-axis all agents lables
    #y-axis the count of the number of times they were given attention
     indx0 = int(col_indx/2)
     indx1 = int(col_indx%2)

     pd.DataFrame(histForClusterX, columns=['attention count']).plot(ax=axes[indx0,indx1],kind='bar',title='Attention Mechanism '+' for cluster'+str(col_indx))
#%%
ut.saveModel(cellPCA_model , "cellPCA_model_10000")
