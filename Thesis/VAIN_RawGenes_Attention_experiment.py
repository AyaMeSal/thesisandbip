# -*- coding: utf-8 -*-
"""
Created on Sat May 19 13:29:34 2018

@author: Aya
"""

# -*- coding: utf-8 -*-
"""
Created on Sat May 19 01:05:05 2018
This experiment test the visualilation of the attention learned When the most variable genes are used as
input to an MLP Model
@author: Aya
"""
import utils as ut
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
from sklearn.decomposition import PCA
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
#%%
import tensorflow as tf
import random as rn
# The below is necessary in Python 3.2.3 onwards to
# have reproducible behavior for certain hash-based operations.
# See these references for further details:
# https://docs.python.org/3.4/using/cmdline.html#envvar-PYTHONHASHSEED
# https://github.com/keras-team/keras/issues/2280#issuecomment-306959926

import os
os.environ['PYTHONHASHSEED'] = '0'

# The below is necessary for starting Numpy generated random numbers
# in a well-defined initial state.

np.random.seed(42)

# The below is necessary for starting core Python generated random numbers
# in a well-defined state.

rn.seed(12345)

# Force TensorFlow to use single thread.
# Multiple threads are a potential source of
# non-reproducible results.
# For further details, see: https://stackoverflow.com/questions/42022950/which-seeds-have-to-be-set-where-to-realize-100-reproducibility-of-training-res

session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)

from keras import backend as K
tf.set_random_seed(1234)

#%%
numEpochs = 1000
#%%
geneExpFile="C:/Data/GE_mvg.csv"
annotationFile="C:/Data/Annotation.csv"
geneNamesFile="C:/Data/CV_genes.csv"
geneNamesDataframe = read_csv(geneNamesFile ,dtype={"gene": str})
geneNames =  list(geneNamesDataframe['gene'])
df = read_csv(geneExpFile, names = geneNames)# ,header = None)
dfStd = df.std()
dfMean = dfStd.mean()
df=df.loc[:, df.std() > (dfMean)]

geneExpression = np.array(df)
Annotations= read_csv(annotationFile)
clusterLabels = Annotations['State']
#%%
scaler = MinMaxScaler()
scaler.fit(geneExpression)
X = scaler.transform(geneExpression)
#one-hot Encoding:
lbls = ut.oneHotEncoding(clusterLabels)
lbls = np.array(lbls)
#%%
X_train, X_test, y_train, y_test = ut.train_test_split(X,lbls, 0.1)
#%% Attention Model
model = ut.build_attention_model(X_train.shape[1],y_train.shape[1])
model , hist_att = ut.compileModel(model , X_train , y_train,2 ,numEpochs)
testAcc_model = model.evaluate(X_test,y_test)
model.summary()
print("Attention Model")
print(model.metrics_names)
print(testAcc_model)
ut.plotHistory(hist_att ,"MLP with attention")