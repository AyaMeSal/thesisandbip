import utils as ut
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
from sklearn.decomposition import PCA
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import MLP_RawGenes_Attention_experiment as synth_MLP
import tensorflow as tf
import random as rn


df = read_csv('./274_genes.csv')# ,header = None)
lbls = read_csv('./Cluster_Lables.csv')

geneExpression = np.array(df)
geneExpression = geneExpression[: ,1:]
#%%
scaler = MinMaxScaler()
scaler.fit(geneExpression)
X = scaler.transform(geneExpression)
#one-hot Encoding:
lbls = np.array(lbls)
lbls=lbls[:,1:]
X , lbls , geneNames = synth_MLP.getData()
#%% VAIN Model
X_train, X_test, y_train, y_test = ut.train_test_split(X,lbls, 0)
singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data_self(X_train , y_train)
#singleton_test , pairwise_test , labels_test ,pairwiseLabels_test = ut.get_data_self(X_test , y_test)

import ThesisModel_AttentionMatrix as tm
#import ThesisModel as t
model = ut.build_attention_model(X_train.shape[1],y_train.shape[1])

model.load_weights("MLP_Attention_Classification_model_best")
#checkpointer = ModelCheckpoint(filepath="/tmp/weights.hdf5", verbose=1, save_best_only=True)
#%%
fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(8, 8))
attention=[]
attention.append([])
attention.append([])
attention.append([])
attention.append([])
attention.append([])

for col_indx in range(0, labels_train.shape[1]):
     #make mask from clusterLabels
     all_col = labels_train[:,col_indx]
     cluster_mask = all_col > 0
     singletonFeaturesForClusterX = singleton_train[cluster_mask]
     index = 0
     for classMember in singletonFeaturesForClusterX:
         singleton = classMember
         attention_vector = ut.getAttentionForAgentSingleInput(singleton ,model)#cellPCA_model)
         
         attention[col_indx].append(attention_vector)
        # histForClusterX += ut.getHistForAttentionVector(attention_vector , othersLabels)
         index+=1
    #plot the graph for this agent
    #x-axis all agents lables
    #y-axis the count of the number of times they were given attention

#%%    
    
att1 =np.matrix ([[0.222938,	0.165593,	0.222938,	0.165593,	0.222938],
[0.170891,	0.243663,	0.170891,	0.243663,	0.170891],
[0.222938,	0.165593,	0.222938,	0.165593,	0.222938],
[0.170891,	0.243663,	0.170891,	0.243663,	0.170891],
[0.222938,	0.165593,	0.222938,	0.165593,	0.222938]])    
fig, ax = plt.subplots(figsize=(20, 20))
fig, ax2 = plt.subplots(figsize=(20, 20))

cellTypes=['Erythrocytes' ,'HSPC' , 'Monocytes' , 'Neutrophils' ,'Thrombocytes']
mean_tolerance = [100 ,2 , 3 ,4 ,5 ,6 ,7 ,8 , 9 , 10 , 11 , 12]
#get the most important genes
#1 get the summation of all the rows corresponding to a cell type 

#2 get the mean of the summed row
#get indices of all the genes that are above average 

attenionOrdered = np.vstack(x for x in attention)
scaler = MinMaxScaler()
scaler.fit(attenionOrdered.T)
X = scaler.transform(attenionOrdered.T)
X = X.T
start = 0 ; end = 152
for i in range(len(cellTypes)):
    
    
    GeneAttentionsForCellX = X[(i+1)*start:(i+1)*end,:]
    GeneAttentionsForCellX_sum = np.sum(GeneAttentionsForCellX , axis =0).reshape(-1,1)
    GeneAttentionsForCellX_mean = np.mean(GeneAttentionsForCellX_sum)
    for tol in mean_tolerance:
        sig_Genes_mask = GeneAttentionsForCellX_sum>tol*GeneAttentionsForCellX_mean
        
        sigGeneNames = []
        index=0
        for isSig in sig_Genes_mask:
            if(isSig):
                sigGeneNames.append(geneNames[index])
            index = index +1
        
        sig = np.array(sigGeneNames) 
        df = pd.DataFrame(sig)
        fileName= cellTypes[i]+"_sigGenes_"+str(tol)+".csv"
        print("set %s has %d sig gense"% (fileName,len(sig)))
        df.to_csv(fileName)
        
scaler.fit(att1)
att = scaler.transform(att1)
ax2.imshow(att,aspect='auto')
ax.imshow(np.array(X),aspect='auto')

plt.show()
#plot the graph for this agent
#x-axis all agents lables
#y-axis the count of the number of times they were given attention
