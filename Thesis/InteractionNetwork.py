#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 02:33:56 2018

@author: a
"""
import keras
from keras.models import Model
from keras.layers import Input, Dense, Flatten, Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization

# =============================================================================
# parameters
# singletonShape:shape of original feature vestor
# pairwiseShape:shape of pairwise feature vector
# outputShape: classes
# =============================================================================
def get_model(singletonShape, pairwiseShape,outputShape):

    singleInputShape = singletonShape # (1,features)
    pairwise_input_shape = pairwiseShape #  (1,2*features)
    nb_classes = outputShape
    hidden_size = 128
    
    singleInp = Input(shape=singleInputShape)
   # flat = Flatten()(singleInp)
    single_dense1 = Dense(hidden_size, activation='relu')(singleInp)
    single_dense1_bn = BatchNormalization(axis=1)(single_dense1)
    single_dense2 = Dense(hidden_size, activation='relu')(single_dense1_bn)
    single_dense2_bn=BatchNormalization(axis=1)(single_dense2)
    x= keras.layers.Reshape((300, 1,128))(single_dense2_bn)
    # singleModel = Model(inputs=singleInpoutputs=x)
    # print(singleModel.summary())

    
    #constructing ModelZ
    pairInp = Input(shape = pairwise_input_shape)
    flat_p = Flatten()(pairInp)
    pair_dense1_1_bn = BatchNormalization(axis=1)(flat_p)
    pair_dense_1_2 = Dense(hidden_size, activation='relu')(pair_dense1_1_bn)
    pair_dense1_2_bn = BatchNormalization(axis=1)(pair_dense_1_2)
    pair_dense1_3_main = Dense(hidden_size, activation='relu')(pair_dense1_2_bn)
    z = pair_dense1_3_main
  #  pair_Z_Model = Model(inputs=pairInp, outputs=z)
   # x = x.reshape(300, 1,128)
    x_z = keras.layers.concatenate([x, z], axis=1)
    beforeLast = Dense(nb_classes, activation='relu')(x_z)
    out = Dense(nb_classes, activation='relu')(beforeLast)
    fullModel = Model(inputs=[singleInp,pairInp], outputs=out)
    print(fullModel.summary())
   
    return fullModel

    
    
    """
   z_att_1 = Dense(1, activation='softmax')(z)
#    z_att_1 = z_att_1.view(-1, 19)
    
    z_main = BatchNormalization(axis=1)(z)
#    z_main = z_main.view(-1, 19, z_main.size(1)) #any number of rows but must be 19 columns and third dimension is
    z_prod = z_main * z_att_1
    z_diff = np.sum(z_prod)#.squeeze()
    """
    

if __name__ == '__main__':

    model = get_model()
