import argparse
import os
import shutil
import time
import numpy as np
import sys

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models
from torch.autograd import Variable
from torch.utils.data.dataset import Dataset
from torch.utils.data import DataLoader
from PIL import Image

import numpy as np
import pickle as pkl
dtype = torch.FloatTensor
# dtype = torch.cuda.FloatTensor # Uncomment this to run on GPU

def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Linear') != -1:
        m.weight.data.normal_(0.0, 0.01)

class _net(nn.Module):
    hiddenCount = 333
    def __init__(self):
        super(_net, self).__init__()
        self.single_dense1 = nn.Linear(5, self.hiddenCount)
        self.single_dense1_bn = nn.BatchNorm1d(self.hiddenCount)
        self.single_dense2 = nn.Linear(self.hiddenCount, self.hiddenCount)
        self.single_dense2_bn = nn.BatchNorm1d(self.hiddenCount)

        self.pair_dense1_1 = nn.Linear(10, self.hiddenCount)
        self.pair_dense1_1_bn = nn.BatchNorm1d(self.hiddenCount)
        self.pair_dense1_2 = nn.Linear(self.hiddenCount, self.hiddenCount)
        self.pair_dense1_2_bn = nn.BatchNorm1d(self.hiddenCount)
        self.pair_dense1_3_main = nn.Linear(self.hiddenCount, self.hiddenCount)
        self.pair_dense1_3_main_bn = nn.BatchNorm1d(self.hiddenCount)
        self.pair_dense1_3_att = nn.Linear(self.hiddenCount, 1)

        self.comb_dense1 = nn.Linear(self.hiddenCount, self.hiddenCount)
        self.comb_dense1_bn = nn.BatchNorm1d(self.hiddenCount)
        #self.comb_dense2 = nn.Linear(256, 256)
        #self.comb_dense2_bn = nn.BatchNorm1d(256)
        #self.comb_dense3 = nn.Linear(256, 256)
        #self.comb_dense3_bn = nn.BatchNorm1d(256)
        #self.comb_dense4 = nn.Linear(256, 256)
        #self.comb_dense4_bn = nn.BatchNorm1d(256)
        self.out_dense = nn.Linear(self.hiddenCount, 4)

        self.relu = nn.ReLU(inplace=True)

    def main(self, datum):
        #datum[0] is the size of the idx of the singleton list
        #datum[1] is the size of

        ##print("datum[0] shape")
        ##print(datum[0].shape)

        x = self.single_dense1(datum[0])
        x = self.single_dense1_bn(x)
        x = self.relu(x)
        x = self.single_dense2(x)
        x = self.single_dense2_bn(x)
        ## print("datum[1].shape")
        ##print(datum[1].shape)
        #print("datum[1].size %d" %datum[1].size())


        z = datum[1].view(-1, datum[1].size(2)) # flatten
        ##print ("After Flatten")
        ##print("datum[1].shape ")
        ##print(datum[1].shape)
        z = self.pair_dense1_1(z)
        z = self.pair_dense1_1_bn(z)
        z = self.relu(z)
        z = self.pair_dense1_2(z)
        z = self.pair_dense1_2_bn(z)
        z = self.relu(z)

        #start of the decodin part


        z_att_1 = self.pair_dense1_3_att(z) #in 256 , out = 1
        # z_att = z_att.view(-1, 31) - 100 * (1.0 - datum[2])
        z_att_1 = z_att_1.view(-1, 19) # flatten # any number of rows but must be 19 columns
        z_att_1 = nn.Softmax()(z_att_1)

        z_main = self.pair_dense1_3_main(z) #in hiddenCount , out hiddenCount
        z_main = self.pair_dense1_3_main_bn(z_main) #in hiddenCount , out hiddenCount
        print("z_main.size(1)")
        print(z_main.size(1))
        z_main = z_main.view(-1, 19, z_main.size(1)) #flatten but in 2D #any number of rows but must be 19 columns and third dimension is PROBABLY hiddencount

        #Picking the right column basically
        #unsqueeze : inserts singleton dimension at position stated as parameter
        #view =creates a view with different dimensions of the storage associated
        #with tensor
        #expand_as : expands this ensor to the size of the specified tensor
        #squeeze() : retrns a tensor with all dimensions of input of size 1 removed
        """
        example: x = torch.Tensor([[1],[2],[3]])
        x.size()
        torch.size([3,1])
        x.expand(3,4)
        1 1 1 1
        2 2 2 2
        3 3 3 3
        [torch.floatTensor of size 3x4]
        """
        # element-wise multiplication
        z_prod = z_main * z_att_1.unsqueeze(2).expand_as(z_main)
        # z_prod = z_main * (datum[2]).unsqueeze(2).expand_as(z_main)
        # z_prod = z_main * z_att.unsqueeze(2).expand_as(z_main)
        z_diff = torch.sum(z_prod, 1).squeeze()
        x = x + z_diff
        x = self.relu(x)

        x = self.comb_dense1(x)
        #3x = self.comb_dense1_bn(x)
        #x = nn.Dropout(p=0.1)(x)
        x = self.relu(x)
        #x = self.comb_dense2(x)
        #x = self.comb_dense2_bn(x)
        #x = nn.Dropout(p=0.1)(x)
        #x = self.relu(x)
        #x = self.comb_dense3(x)
        #x = self.comb_dense3_bn(x)
        #x = nn.Dropout(p=0.1)(x)
        #x = self.relu(x)
        #x = self.comb_dense4(x)
        #x = self.comb_dense4_bn(x)
        #x = nn.Dropout(p=0.1)(x)
        #x = self.relu(x)
        x = self.out_dense(x)
        return x

    def forward(self, input):
        x = self.main(input)
        return x

net = _net()
net.apply(weights_init)
if False:
    net.load_state_dict(torch.load("nets/3laydec.pth"))
print(net)
net.eval()

batch_size = 256
lr = 1e-3 #Learning rate ?
wd = 0e-5

criterion = nn.MSELoss()

input_s = torch.FloatTensor(batch_size, 5)
input_p = torch.FloatTensor(batch_size, 19, 2 * 5)
label_im = torch.FloatTensor(batch_size, 4)
label_0 = torch.FloatTensor(batch_size, 4)
"""
net
criterion
input_s, input_p = input_s, input_p
label_im = label_im
label_0 = label_0
"""

input_s = Variable(input_s)
input_p = Variable(input_p)
label_im = Variable(label_im)
label_0 = Variable(label_0)
label_0.data.fill_(0.0)

def get_data(data):
    print(data.shape)
    simSessions = data.shape[0] #simulation session
    timeFrame = data.shape[1] - 1 # Time less one time step
    agent_n = data.shape[2] # Agents ie. balls
    #initialize the singleton features representation of the balls:
    #one data point with all five features for every agent for
    #every time step over all simulation sessions
    singleton = np.zeros((agent_n * simSessions * timeFrame, 5))
    #initialize the pairwise features representation of the balls:
    #One data point with 10 features, 5 per agent
    #For every agent , depicting the relationship between itself
    #and each other agent
    #for every time step over all simulation sessions
    pairwise = np.zeros((agent_n * simSessions * timeFrame, agent_n - 1, 10))
    #initialize the prediction of the features
    #for every set of features at a time point , predict the
    #CHANGE in X, Y position Vertical and horizontal velocity
    #between time steps
    output = np.zeros((agent_n * simSessions * timeFrame, 4))
    for session in range(simSessions):
        for j in range(timeFrame):
            for aid in range(agent_n):
                idx = agent_n * (session * (timeFrame - 1) + j) + aid
                singleton[idx] = X[session, j, aid, :]
                for aid_id, aid_others in enumerate([x for x in range(agent_n) if x != aid]):
                    pairwise[idx, aid_id, :5] = X[session, j, aid, :]
                    pairwise[idx, aid_id, 5:] = X[session, j, aid_others, :]
                output[idx] = X[session, j + 1, aid, :4] - X[session, j, aid, :4]

    idx = np.where(np.sum(singleton**2, axis=1) < 10)[0]
    singleton = singleton[idx]
    pairwise = pairwise[idx]
    output = output[idx]
    return [singleton, pairwise, output]

import math
X = np.load('X.npy')

datasize=X.shape[0]
trainingDataPart = math.floor(0.8*datasize)
validationDataPart = math.floor(0.9*datasize)
print(datasize)
train_x = X[:trainingDataPart]
train_data = get_data(train_x)
train_data[0].shape
train_data[1].shape

#print(train_data[2][:10,:])
rp = np.random.permutation(len(train_data[0]))
train_data = [torch.from_numpy(train_data[0][rp]),
        torch.from_numpy(train_data[1][rp]),
        torch.from_numpy(train_data[2][rp])]
val_x = X[trainingDataPart:validationDataPart]
val_data = get_data(val_x)
val_data = [torch.from_numpy(val_data[0]),
        torch.from_numpy(val_data[1]),
        torch.from_numpy(val_data[2])]
test_x = X[validationDataPart:]
test_data = get_data(test_x)
# setup optimizer
optimizer = optim.Adam(net.parameters(), lr=lr, betas=(0.5, 0.999), weight_decay=wd)

#Running the Training and testing
import math
for epoch in range(100):
    iter_n = len(train_data[0]) / batch_size
    train_err_s = 0.0
    net.train()
    for i in range(math.floor(iter_n)):
        s_data = train_data[0][i * batch_size: i * batch_size + batch_size]
        p_data = train_data[1][i * batch_size: i * batch_size + batch_size]
        dim_data = train_data[2][i * batch_size: i * batch_size + batch_size]

        input_s.data.resize_(s_data.size()).copy_(s_data)
        input_p.data.resize_(p_data.size()).copy_(p_data)
        label_im.data.resize_(dim_data.size()).copy_(dim_data)

        net.zero_grad()
        z = net([input_s, input_p])
        err = criterion(z, label_im)
        err.backward()
        optimizer.step()
        train_err_s += err.data[0]
    train_err_s /= iter_n
    iter_n = len(test_data[0]) / batch_size
    test_err_s = 0.0
    test_err0_s = 0.0
    # net.eval()
    #Testing against validation data for evert epoch
    for i in range(math.floor(iter_n)):
        s_data = val_data[0][i * batch_size: i * batch_size + batch_size]
        p_data = val_data[1][i * batch_size: i * batch_size + batch_size]
        dim_data = val_data[2][i * batch_size: i * batch_size + batch_size]

        input_s.data.resize_(s_data.size()).copy_(s_data)
        input_p.data.resize_(p_data.size()).copy_(p_data)
        label_im.data.resize_(dim_data.size()).copy_(dim_data)

        z = net([input_s, input_p])
        err = criterion(z, label_im)
        err0 = criterion(label_im, label_0)
        test_err_s += err.data[0]
        test_err0_s += err0.data[0]
    test_err_s /= iter_n
    test_err0_s /= iter_n
    print("Epoch: %d Train Error: %f Test Error: %f Baseline Error: %f"%(epoch, 1e6 * train_err_s, 1e6 * test_err_s, 1e6 * test_err0_s))
    torch.save(net.state_dict(), 'nets/net_%02d.pth'%epoch)
