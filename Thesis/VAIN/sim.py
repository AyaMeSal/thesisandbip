import numpy as np

n = 20 #number of balls ?
T = 100 # time steps ?
G = 0.1
dt = 0.05
M = 3 #Bouncing session 

#where is the size of the 2D contianer?

X = np.zeros((M, T, n, 5))
for sess in range(M): # for every simulation session 
    x = 1 * np.random.rand(n, 2)  # randomly initialize the starting positions
    v = 0.0 * np.random.rand(n, 2) # randomly initialize the initial velocities
    m = np.random.rand(n)   # randomly initialize mass  for ever ball

    for t in range(T):  #for every time step
        a = np.zeros((n,2)) # a (assuming acceleraion) initialized to 0
        for i in range(n): # for every agent in the set of balls
            dx = x[i:i+1, :] - x    #calculate displacement (delta x)
            r = np.sqrt(np.sum(dx*dx, axis=1))[:, None]
            if False:
                da = - G * m[:, None] * dx / (r ** 3 + 1e-8)
            else:
                da = - G * dx
            da[i, :] = 0
            a[i] = da.sum(axis=0)
        x += v * dt + 0.5 * a * dt * dt
        v += a * dt
        X[sess, t, :, :2] = x  # X , y coordinates 
        X[sess, t, :, 2:4] = v # velocity in the X and Y directions
        X[sess, t, :, 4] = m #mass
np.save('X_3', X)
