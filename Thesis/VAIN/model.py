from keras.models import Model
from keras.layers import Input, Dense, Flatten, Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
import utils as ut

def get_model():

    # Model parameters
    rows, cols = 28 , 28
    input_shape = (rows, cols, 1)

    nb_classes = 10

    hidden_size = 128

    inp = Input(shape=input_shape)
    #flat = Flatten()(inp)
    conv1 = Conv2D(16, (3, 3), activation='relu', padding='same')(inp)
    BN0=BatchNormalization(axis=1)(conv1)
    maxp1 = MaxPooling2D((2, 2), padding='same')(BN0)
    BN1=BatchNormalization(axis=1)(maxp1)
    flat = Flatten()(BN1)
   # keras.layers.normalization.BatchNormalization(epsilon=1e-06, mode=0, momentum=0.9, weights=None)

    hidden_1 = Dense(hidden_size, activation='relu')(flat)
    BN2=BatchNormalization(axis=1)(hidden_1)

    hidden_2 = Dense(hidden_size, activation='sigmoid')(BN2)
    out = Dense(nb_classes, activation='softmax')(hidden_2)

    model = Model(inputs=inp, outputs=out)

    print(model.summary())

    return model


if __name__ == '__main__':

    model = get_model()
    ut.drawModel(model)
