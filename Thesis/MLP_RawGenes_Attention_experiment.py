# -*- coding: utf-8 -*-
"""
Created on Sat May 19 01:05:05 2018
This experiment test the visualilation of the attention learned When the most variable genes are used as
input to an MLP Model
@author: Aya
"""
import utils as ut
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
from sklearn.decomposition import PCA
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
#%%
import tensorflow as tf
import random as rn
# The below is necessary in Python 3.2.3 onwards to
# have reproducible behavior for certain hash-based operations.
# See these references for further details:
# https://docs.python.org/3.4/using/cmdline.html#envvar-PYTHONHASHSEED
# https://github.com/keras-team/keras/issues/2280#issuecomment-306959926

import os
os.environ['PYTHONHASHSEED'] = '0'

# The below is necessary for starting Numpy generated random numbers
# in a well-defined initial state.

np.random.seed(42)

# The below is necessary for starting core Python generated random numbers
# in a well-defined state.

rn.seed(12345)

# Force TensorFlow to use single thread.
# Multiple threads are a potential source of
# non-reproducible results.
# For further details, see: https://stackoverflow.com/questions/42022950/which-seeds-have-to-be-set-where-to-realize-100-reproducibility-of-training-res

session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)

from keras import backend as K
import keras
tf.set_random_seed(1234)

#%%


#%%
run = 0
numEpochs = 100
def getData():
    geneExpFile="C:/Users/Aya/bitBucketRepo/cleanCheckout/thesisandbip/Thesis/Data/GE_mvg_balanced.csv"
    #annotationFile="C:/Data/Annotation.csv"
    #Annotations= read_csv(annotationFile)
    
    df = read_csv(geneExpFile)
    geneExpression = np.array(df)
    geneExpression = geneExpression[:,1:] #remove type
    
    
    
    geneNames =  list(df)
    
    #%%
    scaler = MinMaxScaler()
    scaler.fit(geneExpression)
    X = scaler.transform(geneExpression)
    #one-hot Encoding:
    lbls = read_csv('./Cluster_Lables.csv')
    lbls = np.array(lbls)
    lbls = lbls[:,1:]
    clusterLabels = df['Type']
    lbls = ut.oneHotEncoding(clusterLabels)
    lbls= np.array(lbls)
    return X,lbls,geneNames

X , lbls , geneNames = getData()
#%%
if run ==1:
    X_train, X_test, y_train, y_test = ut.train_test_split(X,lbls, 0.1)
    saveBest = keras.callbacks.ModelCheckpoint("MLP_Attention_Classification_model_best", monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)
    
    #%% Attention Model
    model = ut.build_attention_model(X_train.shape[1],y_train.shape[1])
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=[keras.metrics.categorical_accuracy])
    hist_att = model.fit(X_train, y_train, epochs=numEpochs ,validation_split = 0.1, verbose=2, callbacks = [saveBest])
    testAcc_model = model.evaluate(X_test,y_test)
    
    
    ut.saveModel(model , "MLP_Attention_Classification_model_27May_"+str(numEpochs))
    model.summary()
    print("Attention Model")
    print(model.metrics_names)
    print(testAcc_model)
    ut.plotHistory(hist_att ,"MLP with attention")
    #%%
    y_train = np.array(y_train)
    X_train = np.array(X_train)
    fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(80, 80))
    for col_indx in range(0, y_train.shape[1]):
         #make mask from clusterLabels
         hist = [0,0,0,0,0]
         all_col = y_train[:,col_indx]
         cluster_mask = all_col > 0
         singletonFeaturesForClusterX = X_train[cluster_mask]
         LablesforOtherAgents = y_train[cluster_mask]
         histForClusterX = [0]* X_train.shape[1]
         index = 0
         for classMember in singletonFeaturesForClusterX:
             singleton = classMember
             othersLabels = LablesforOtherAgents[index]
             attention_vector = ut.getAttentionForAgentSingleInput(singleton , model)#cellPCA_model)
             histForClusterX += attention_vector
             index+=1
        #plot the graph for this agent
        #x-axis all agents lables
        #y-axis the count of the number of times they were given attention
         indx0 = int(col_indx/2)
         indx1 = int(col_indx%2)
    
         pd.DataFrame(histForClusterX,list(df.columns.values) , columns=['attention count']).plot(ax=axes[indx0,indx1],kind='bar',title='Attention Mechanism '+' for cluster'+str(col_indx))
