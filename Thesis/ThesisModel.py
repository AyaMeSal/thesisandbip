#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 02:33:56 2018

@author: a
"""
import keras
from keras.models import Model
from keras.layers import Input, merge, Dense, Flatten, Conv1D, Softmax , Lambda , multiply
from keras.layers.normalization import BatchNormalization
import keras.backend as K
import utils as ut
"""
convLayer_x = Conv1D(filters =1 , kernel_size = 1 , activation = 'relu')(inp)
    K.eval(convLayer_x)
   # x_reshape = keras.backend.reshape(convLayer_x, (1,1,5))
    K.eval(x_reshape)
    convLayer_x = K.flatten(convLayer_x)

    attention_probs = Dense(numagentsLess1, activation='softmax', name='attention_vec')
    K.eval(attention_probs(x_reshape))
    sm = K.softmax(convLayer_x) # softmaxed probability
    K.eval(sm)
    sm.shape
    (K.transpose(sm)).shape
    #now multiply with original input
    convLayer_flt = keras.backend.expand_dims(sm, axis=-1)
    d = K.dot(K.transpose(convLayer_flt),inp  )
    mul = convLayer_flt * inp
    K.eval(mul)
    mul.shape
    #summation over all agents
    augmented = K.sum(mul , axis =1)
    K.eval(augmented)
    augmented.shape
"""

def get_full_model(features, numagents,labels_shape):

    singleInputShape = features
    pairwise_input_shape = singleInputShape * 2
    numAgentsLess1 = numagents - 1
    nb_classes =  labels_shape
    hidden_size = 128

    singleInp = Input(shape=(singleInputShape,))
    x = Dense(hidden_size, activation='relu')(singleInp)
    x = BatchNormalization(axis=1)(x)
    x = Dense(hidden_size, activation='relu')(x)
    x = BatchNormalization(axis=1)(x)
   # x = single_dense2_bn
   # singleModel = Model(inputs=singleInp, outputs=x)
   # print(singleModel.summary())

    pairInp = Input(shape = (numAgentsLess1,pairwise_input_shape,))

    ##
    convLayer_x = Conv1D(filters =1 , kernel_size = 1 , activation = 'relu')(pairInp)
#    K.eval(convLayer_x)
   # x_reshape = keras.backend.reshape(convLayer_x, (1,1,5))
   # K.eval(x_reshape)
    convLayer_x = Flatten()(convLayer_x)
   # attention_probs = Dense(numagentsLess1, activation='softmax', name='attention_vec')
    #K.eval(attention_probs(x_reshape))
    sm =Lambda(lambda x : K.softmax(x) ,name='attention_vec') (convLayer_x) # , output_shape = (1 , pairwise_input_shape))(convLayer_x)
    #sm = Softmax(convLayer_x)# axis =-1)
   # K.eval(sm)
    #sm.shape
    #(K.transpose(sm)).shape
    #now multiply with original input

    ##convLayer_flt = keras.backend.expand_dims(sm, axis=-1)
    sm = Lambda(lambda x :keras.backend.expand_dims(x, axis=-1) )(sm)
    #print(sm.output_shape)
    # d = K.dot(K.transpose(convLayer_flt),inp  )
    #mul = convLayer_flt * pairInp
    #mul= Lambda(lambda x:  x * pairInp)(sm)
    #from keras.layers import multiply
    mul = multiply([pairInp, sm])
   # K.eval(mul)
   # mul.shape
    #summation over all agents
    #augmented = K.sum(mul , axis =1)
    augmented = Lambda(lambda xin: K.sum(xin, axis=1))(mul) #, output_shape=(1,pairwise_input_shape))(mul) # adjust shape
    augmented.shape

    inp =augmented
    pairs_encoded = Dense(hidden_size, activation='relu')(inp)
    pairs_encoded = BatchNormalization(axis=1)(pairs_encoded)
    pairs_encoded = Dense(hidden_size, activation='relu')(pairs_encoded)
    pairs_encoded = BatchNormalization(axis=1)(pairs_encoded)
    pairs_encoded.shape
    ##




    """ # ATTENTION PART STARTS HERE
    attention_probs = Dense(pairwise_input_shape, activation='softmax', name='attention_vec')(pairInp)
    attention_mul = merge([pairInp, attention_probs], output_shape=input_dim, name='attention_mul', mode='mul')
    """# ATTENTION PART FINISHES HERE
#    zWithAtt = Dense(14 , activation='sigmoid')(attention_mul)
   # output = Dense(num_classes , activation='sigmoid')(attention_mul)
    #model = Model(input=[inputs], output=output)
    #return model
    x_z_concatenated = keras.layers.add([x, pairs_encoded])
    beforeLast = Dense(nb_classes, activation='softmax')(x_z_concatenated)
    out = Dense(nb_classes, activation='relu')(beforeLast)

    fullModel = Model(inputs=[singleInp,pairInp], outputs=out)
    print(fullModel.summary())
    return fullModel



def get_model(single_shape, pairs_shape,labels_shape):

    rows, cols = 28 , 28
    features = 5
    singleInputShape = single_shape
    pairwise_input_shape =  pairs_shape

    nb_classes=labels_shape
    hidden_size = 128

    singleInp = Input(shape=(singleInputShape,))
    x = Dense(hidden_size, activation='relu')(singleInp)
    x = BatchNormalization(axis=1)(x)
    x = Dense(hidden_size, activation='relu')(x)
    x = BatchNormalization(axis=1)(x)
   # x = single_dense2_bn
   # singleModel = Model(inputs=singleInp, outputs=x)
   # print(singleModel.summary())

    pairInp = Input(shape = (pairwise_input_shape,))
    # ATTENTION PART STARTS HERE
    attention_probs = Dense(pairwise_input_shape, activation='softmax', name='attention_vec')(pairInp)
    attention_mul = merge([pairInp, attention_probs], output_shape=input_dim, name='attention_mul', mode='mul')
    # ATTENTION PART FINISHES HERE
    zWithAtt = Dense(14 , activation='sigmoid')(attention_mul)
   # output = Dense(num_classes , activation='sigmoid')(attention_mul)
    #model = Model(input=[inputs], output=output)
    #return model
    x_z_concatenated = keras.layers.concatenate([x , zWithAtt]) #also try add
    beforeLast = Dense(nb_classes, activation='softmax')(x_z_concatenated)
    out = Dense(nb_classes, activation='relu')(beforeLast)
    fullModel = Model(inputs=[singleInp,pairInp], outputs=out)
    print(fullModel.summary())
    return fullModel


    #constructing ModelZ
"""
    pairInp = Input(shape= pairwise_input_shape)
    pair_dense1_1_bn = BatchNormalization(axis=1)(pairInp)
    pair_dense_1_2 = Dense(hidden_size, activation='relu')(pair_dense1_1_bn)
    pair_dense1_2_bn = BatchNormalization(axis=1)(pair_dense_1_2)
    pair_dense1_3_main = Dense(hidden_size, activation='relu')(pair_dense1_2_bn)
    z = pair_dense1_3_main
  #  pair_Z_Model = Model(inputs=pairInp, outputs=z)
    x_z = keras.layers.Add()([x, z])
    beforeLast = Dense(nb_classes, activation='softmax')(x_z)
    out = Dense(nb_classes, activation='relu')(beforeLast)

    fullModel = Model(inputs=[singleInp,pairInp], outputs=out)
    print(fullModel.summary())
"""



input_dim = 32



"""
   z_att_1 = Dense(1, activation='softmax')(z)
#    z_att_1 = z_att_1.view(-1, 19)

    z_main = BatchNormalization(axis=1)(z)
#    z_main = z_main.view(-1, 19, z_main.size(1)) #any number of rows but must be 19 columns and third dimension is
    z_prod = z_main * z_att_1
    z_diff = np.sum(z_prod)#.squeeze()
"""


if __name__ == '__main__':

    model = get_full_model(5 ,10 , 5)
    model.summary()
    from IPython.display import SVG
    from keras.utils.vis_utils import model_to_dot
#    SVG(model_to_dot(model).create(prog='dot', format='svg'))

