# -*- coding: utf-8 -*-
"""
Created on Fri May 18 01:06:43 2018

@author: Aya
"""
#get the data
#split up the data
#get the model
# run the model
import utils as ut
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
from sklearn.decomposition import PCA
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
import numpy as np
#%%
numEpochs = 1
#%%
geneExpFile='C:/Data/cells_thrombocytes_273.xlsx'
df = read_csv(geneExpFile)# ,header = None)
#%%
genesArray = np.array(df)
genesArray = genesArray[:,1:]

#variances = (np.std(combinedDFasArray ,axis = 0))
#df.sort_values(variances.argsort(), axis=1)
#keep only highest 500 std

scaler = MinMaxScaler()
scaler.fit(genesArray)
genesArray = scaler.transform(genesArray)
#%%
geneExpInput = genesArray[:-1 ,:]
#geneExpInput= np.expand_dims((geneExpInput), axis=1)
lbls = genesArray[1: ,:]
#lbls= np.expand_dims((geneExpOutput), axis=1)

X_train, X_test, y_train, y_test = ut.train_test_split(geneExpInput,lbls, 0.005)
singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data_self(X_train , y_train)
singleton_test , pairwise_test , labels_test ,pairwiseLabels_test = ut.get_data_self(X_test , y_test)


#geneExpInput, X_test, geneExpOutput, y_test = ut.train_test_split(geneExpInput,geneExpOutput, 0.2)
import ThesisModel_AttentionMatrix as tm
from keras import metrics

#import ThesisModel as t
import keras
adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)#, amsgrad=False)
VAIN_Evolution  = tm.get_full_model(singleton_train.shape[1],singleton_train.shape[0],lbls.shape[1])
VAIN_Evolution.compile(loss='mean_squared_error', optimizer= adam, metrics=[metrics.mae])

epochcount = 1
try:
    while True:
        VAIN_Evolution_History = VAIN_Evolution.fit([singleton_train , pairwise_train], labels_train, batch_size = singleton_train.shape[0],epochs=epochcount ,validation_split = 0.2, verbose=2)# , callbacks =[tensorboard])# , trace.attention()])
        print("Epoch number %d" %epochcount)
        epochcount= epochcount+1
except KeyboardInterrupt:
	pass

ut.saveModel(VAIN_Evolution , "VAIN_Thrombo_Evolution"+str(numEpochs))

"""
model = AttentionSeq2Seq(input_dim=geneExpInput.shape[2], input_length=geneExpInput.shape[1], hidden_dim=10, output_length=geneExpOutput.shape[1], output_dim=geneExpOutput.shape[2], depth=4)
model.compile(loss='mse', optimizer='adam')
history = model.fit(geneExpInput, geneExpOutput, epochs=numEpochs , verbose=2)
testSample = np.array(geneExpInput[1]).reshape(1,1,geneExpInput.shape[2])
prediction = geneExpOutput[1]
print(prediction)
model.predict(testSample)#,geneExpOutput[1])
"""

