# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 14:40:58 2018

@author: Aya
"""
import numpy as np
np.random.seed(1337)
import InteractionNetwork as IN
import tensorflow as tf

from keras.models import *
from keras.layers import *
import keras
import pandas as pd
from pandas import read_csv
from sklearn.datasets import load_breast_cancer
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import mglearn , sklearn
from sklearn.model_selection import train_test_split
#from IPython.display import SVG
from keras.utils.vis_utils import model_to_dot
from keras.layers import Conv2D, Input ,Conv1D
import attention_utils as au


from time import time
from keras.callbacks import TensorBoard
import my_callbacks as trace

tensorboard = TensorBoard(log_dir="logs/{}".format(time()))

#tensor.shape.eval()
def train_test_split(X,Y,split , shuff):

    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(X,Y, test_size=split , shuffle = shuff )
    return X_train, X_test, y_train, y_test

def fixSeeds():
    import numpy as np
    import tensorflow as tf
    import random as rn

    # The below is necessary in Python 3.2.3 onwards to
    # have reproducible behavior for certain hash-based operations.
    # See these references for further details:
    # https://docs.python.org/3.4/using/cmdline.html#envvar-PYTHONHASHSEED
    # https://github.com/keras-team/keras/issues/2280#issuecomment-306959926

    import os
    os.environ['PYTHONHASHSEED'] = '0'

    # The below is necessary for starting Numpy generated random numbers
    # in a well-defined initial state.

    np.random.seed(42)

    # The below is necessary for starting core Python generated random numbers
    # in a well-defined state.

    rn.seed(12345)

    # Force TensorFlow to use single thread.
    # Multiple threads are a potential source of
    # non-reproducible results.
    # For further details, see: https://stackoverflow.com/questions/42022950/which-seeds-have-to-be-set-where-to-realize-100-reproducibility-of-training-res

    session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)

    from keras import backend as K

    # The below tf.set_random_seed() will make random number generation
    # in the TensorFlow backend have a well-defined initial state.
    # For further details, see: https://www.tensorflow.org/api_docs/python/tf/set_random_seed

    tf.set_random_seed(1234)

    sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
    K.set_session(sess)

def getAttentionForAgent( singleton_sample, pairwise_sample,model):
    singleton_sample = np.expand_dims((singleton_sample), axis=0)
    pairwise_sample = np.expand_dims((pairwise_sample), axis=0)
    attention_vector = au.get_activations_simple(model,singleton_sample,pairwise_sample, print_shape_only=True,layer_name='attention_vec')
    attention_vector= attention_vector[0].flatten()
    return attention_vector
def getAttentionForAgentSingleInput( singleton_sample,model):
    singleton_sample = np.expand_dims((singleton_sample), axis=0)
    attention_vector = au.get_activations_simple_SingleInput(model,singleton_sample, print_shape_only=True,layer_name='attention_vec')
    attention_vector= attention_vector[0].flatten()
    return attention_vector
def build1DConvNetwork():
    inputs = Input(shape = (19,10,))
    x = Conv1D(filters =1 , kernel_size = 19 , activation = 'relu')(inputs)

def build2DInputNetwork():#input_dim ,num_classes):

    inputs = Input(shape = (10,))
    x = BatchNormalization(axis=1)(inputs)
    x = Dense(10 , activation = 'relu' , name ='startConv')(x)
    x = BatchNormalization(axis=1)(x)
    output = Dense(1 , activation = 'relu' , name ='output')(x)
    model = Model(input=[inputs], output=output)
    return model



def build_residual_connection_model(input_dim ,num_classes):
    # input tensor for a 3-channel 256x256 image
    x = Input(shape=(input_dim,))
    y = Dense(14 , activation='sigmoid')(x)
    # this returns x + y.
    z = keras.layers.add([x, y])
    output = Dense(num_classes , activation='sigmoid')(z)
    model = Model(input=[inputs], output=output)
    return model


def drawModel(model):

   # SVG(model_to_dot(model).create(prog='dot', format='svg'))
	return

def getHistForAttentionVector(attention_vector, labels_of_other_agents):
    mean = np.mean(attention_vector)
    mask = attention_vector>mean
    labels_of_other_agents = labels_of_other_agents[mask]
    histogram_other_labels = np.sum(labels_of_other_agents, axis = 0)
    return histogram_other_labels
	
def isNullData(dataframe):
    dataframe.isnull().any()
    
def loadMultiVariateDistMuCov(rows, cols, mu, cov):
    #Second Variable is variance
    dataSet = np.random.multivariate_normal(mu, np.eye(cols), size= rows)
    return dataSet    

def loadMultiVariateDist(rows , cols):
    #Second Variable is variance
    dataSet = np.random.multivariate_normal(np.zeros(cols), np.eye(cols), size= rows)
    return dataSet

def writeToFile(data):
    return 0
#Make Data
def get_data_self(data, labels):
    print(data.shape)
   # Time less one time step
    agent_n = data.shape[0] # Agents ie. balls
    features =  data.shape[1]
    #initialize the singleton features representation of the balls:
    #one data point with all  features for every agent for
    #every time step over all simulation sessions
    singleton = np.zeros((agent_n , features))
    #initialize the pairwise features representation of the balls:
    #One data point with 10 features, 5 per agent
    #For every agent , depicting the relationship between itself
    #and each other agent
    #for every time step over all simulation sessions
    pairwise = np.zeros((agent_n, agent_n, features*2))
    #initialize the prediction of the features
    #for every set of features at a time point , predict the
    #CHANGE in X, Y position Vertical and horizontal velocity
    #between time steps
    #output = np.zeros((agent_n, nb_classes))
    num_clusters = labels.shape[1]
    pairwise_labels = np.zeros((agent_n, agent_n, num_clusters))
    for aid in range(agent_n):
        singleton[aid] = data[aid, :]
        for aid_id, aid_others in enumerate([x for x in range(agent_n)]):
            pairwise[aid, aid_id, :features] = data[aid, :]
            pairwise[aid, aid_id, features:] = data[aid_others, :]
            pairwise_labels[aid , aid_id , :num_clusters ] = labels[aid_others]
    """
    idx = np.where(np.sum(singleton**2, axis=1) < 10)[0]
    singleton = singleton[idx]
    pairwise = pairwise[idx]
    output = output[idx]
    """
    return singleton, pairwise, labels , pairwise_labels


def get_data(data, labels):
    print(data.shape)
   # Time less one time step
    agent_n = data.shape[0] # Agents ie. balls
    features =  data.shape[1]
    #initialize the singleton features representation of the balls:
    #one data point with all  features for every agent for
    #every time step over all simulation sessions
    singleton = np.zeros((agent_n , features))
    #initialize the pairwise features representation of the balls:
    #One data point with 10 features, 5 per agent
    #For every agent , depicting the relationship between itself
    #and each other agent
    #for every time step over all simulation sessions
    pairwise = np.zeros((agent_n, agent_n - 1, features*2))
    #initialize the prediction of the features
    #for every set of features at a time point , predict the
    #CHANGE in X, Y position Vertical and horizontal velocity
    #between time steps
    #output = np.zeros((agent_n, nb_classes))
    num_clusters = labels.shape[1]
    pairwise_labels = np.zeros((agent_n, agent_n - 1, num_clusters))
    for aid in range(agent_n):
        singleton[aid] = data[aid, :]
        for aid_id, aid_others in enumerate([x for x in range(agent_n) if x != aid]):
            pairwise[aid, aid_id, :features] = data[aid, :]
            pairwise[aid, aid_id, features:] = data[aid_others, :]
            pairwise_labels[aid , aid_id , :num_clusters ] = labels[aid_others]
    """
    idx = np.where(np.sum(singleton**2, axis=1) < 10)[0]
    singleton = singleton[idx]
    pairwise = pairwise[idx]
    output = output[idx]
    """
    return singleton, pairwise, labels , pairwise_labels
def get_data2(data, labels):
    print(data.shape)
   # Time less one time step
    agent_n = data.shape[0] # Agents ie. balls
    features =  data.shape[1]
    #initialize the singleton features representation of the balls:
    #one data point with all  features for every agent for
    #every time step over all simulation sessions
    singleton = np.zeros((agent_n , features))
    #initialize the pairwise features representation of the balls:
    #One data point with 10 features, 5 per agent
    #For every agent , depicting the relationship between itself
    #and each other agent
    #for every time step over all simulation sessions
    pairwise = np.zeros((agent_n *agent_n , features*2))
    #initialize the prediction of the features
    #for every set of features at a time point , predict the
    #CHANGE in X, Y position Vertical and horizontal velocity
    #between time steps
    #output = np.zeros((agent_n, nb_classes))
    indx = 0
    num_clusters = labels.shape[1]
    pairwise_labels = np.zeros((agent_n, agent_n - 1, num_clusters))
    for aid in range(0,agent_n):
        singleton[aid] = data[aid, :]
        for aid_id in range(0,agent_n):
            pairwise[indx, :features] = data[aid, :]
            pairwise[indx, features:] = data[aid_id, :]
            indx+=1
            #pairwise_labels[aid , aid_id , :num_clusters ] = labels[aid_others]
    """
    idx = np.where(np.sum(singleton**2, axis=1) < 10)[0]
    singleton = singleton[idx]
    pairwise = pairwise[idx]
    output = output[idx]
    """
    return singleton, pairwise, labels , pairwise_labels

def plotHistoryloss(history,title):
    print(history.history.keys())
    keys = list(history.history.keys())
    # summarize history for loss
    plt.plot(history.history[keys[1]])
    plt.plot(history.history[keys[0]])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()


def plotHistory(history,title):
    print(history.history.keys())
    keys = list(history.history.keys())
    # summarize history for accuracy
    plt.plot(history.history[keys[3]])
    plt.plot(history.history[keys[1]])
    plt.title( title +' model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
    # summarize history for loss
    plt.plot(history.history[keys[2]])
    plt.plot(history.history[keys[0]])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
def compileModel(model , X_train , y_train,verboze ,epochsNum):
# Compile model
   model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=[keras.metrics.categorical_accuracy])
   #model.summary()
   history = model.fit(X_train, y_train, epochs=epochsNum ,validation_split = 0.33, verbose=verboze)# , callbacks = [earlyStopping])
   return model , history

def build_attention_model(input_dim ,num_classes):
    import math
    inputs = Input(shape = (input_dim,))
    # ATTENTION PART STARTS HERE
    attention_probs = Dense(input_dim, activation='softmax', name='attention_vec')(inputs)
    attention_mul = merge([inputs, attention_probs], output_shape=input_dim, name='attention_mul', mode='mul')
    # ATTENTION PART FINISHES HERE
   # attention_mul = Dense(math.floor(input_dim*0.7) , activation='sigmoid', kernel_initializer='zeros',
    #            bias_initializer='zeros')(attention_mul)
    attention_mul = Dense(math.floor(input_dim*0.5) , activation='relu')(attention_mul)
  #  attention_mul = Dense(math.floor(input_dim*0.3) , activation='sigmoid' , kernel_initializer='zeros',
  #              bias_initializer='zeros')(attention_mul)
    attention_mul = Dense(math.floor(input_dim*0.1) , activation='relu')(attention_mul)
    output = Dense(num_classes , activation='softmax')(attention_mul)
    model = Model(input=[inputs], output=output)
    return model

def constructAutoEncoder(input_dim ,num_classes):
    import math
    inputs = Input(shape = (input_dim,))
    attention_mul=BatchNormalization(axis=1)(inputs)
    attention_mul = Dense(math.floor(input_dim*1.5) , activation='sigmoid', kernel_initializer='zeros',
                bias_initializer='zeros')(inputs)
    attention_mul=BatchNormalization(axis=1)(attention_mul)
    attention_mul = Dense(math.floor(input_dim*0.5) , activation='sigmoid',kernel_initializer='zeros',
                bias_initializer='zeros')(attention_mul)
    attention_mul=BatchNormalization(axis=1)(attention_mul)

   # attention_mul = Dense(math.floor(input_dim*0.3) , activation='sigmoid' , kernel_initializer='zeros',
    #            bias_initializer='zeros')(attention_mul)
   # attention_mul = Dense(math.floor(input_dim*0.1) , activation='sigmoid',kernel_initializer='zeros',
    #            bias_initializer='zeros')(attention_mul)
    #attention_mul = Dense(math.floor(input_dim*0.3) , activation='sigmoid' , kernel_initializer='zeros',
     #           bias_initializer='zeros')(attention_mul)
    attention_mul = Dense(math.floor(input_dim*1.5) , activation='sigmoid',kernel_initializer='zeros',
                bias_initializer='zeros')(attention_mul)
    attention_mul = BatchNormalization(axis=1)(attention_mul)

   # attention_mul = Dense(math.floor(input_dim*0.7) , activation='sigmoid', kernel_initializer='zeros',
    #            bias_initializer='zeros')(attention_mul)
    output = Dense(num_classes , activation='tanh' ,kernel_initializer='zeros',
                bias_initializer='zeros')(attention_mul)
    model = Model(input=[inputs], output=output)
    return model

def compileRegression(model , input1 , input2 ,output,verboze,epochsNum):
    sgd = keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(loss='mse', optimizer= adam) #, metrics=['accuracy'])
    history = model.fit([input1 , input2], output, epochs=epochsNum ,validation_split = 0.2, verbose=verboze)
    return model , history

def compileAutoEncoder(model , X_train , y_train,verboze,epochsNum):
# Compile model
   sgd = keras.optimizers.SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
   model.compile(loss='mse', optimizer=sgd, metrics=['accuracy'])
   #model.summary()
   model.fit(X_train, y_train,batch_size=X_train.shape[0], epochs=epochsNum ,validation_split = 0.33, verbose=verboze)# , callbacks = [earlyStopping])
   return model

def constructNeuralNet(input_dim ,num_classes):
    import math
    inputs = Input(shape = (input_dim,))
    #attention_mul = Dense(math.floor(input_dim*0.7) , activation='sigmoid', kernel_initializer='zeros',
    #            bias_initializer='zeros')(inputs)
    attention_mul = Dense(math.floor(input_dim*0.5) , activation='sigmoid',kernel_initializer='zeros',
                bias_initializer='zeros')(inputs)
  #  attention_mul = Dense(math.floor(input_dim*0.3) , activation='sigmoid' , kernel_initializer='zeros',
   #             bias_initializer='zeros')(attention_mul)
    attention_mul = Dense(math.floor(input_dim*0.1) , activation='sigmoid',kernel_initializer='zeros',
                bias_initializer='zeros')(attention_mul)
    output = Dense(num_classes , activation='softmax' ,kernel_initializer='zeros',
                bias_initializer='zeros')(attention_mul)
    model = Model(input=[inputs], output=output)
    return model
def selectFeatures():
    print(np.var(results))
def SelectFeaturesRFE(features , labels):
    from sklearn.feature_selection import RFE
    from sklearn.linear_model import LogisticRegression
    model = LogisticRegression()
    rfe = RFE(model, 3)
    fit = rfe.fit(X, Y)
    print("Num Features: %d" % fit.n_features_)
    print("Selected Features: %s" % fit.support_)
    print("Feature Ranking: %s" % fit.ranking_)
def constructInteractionNeuralNet(singletonShape, pairwiseShape,outputShape):
    model = IN.get_model(singletonShape, pairwiseShape,outputShape)
    return model

def compileIntractioneModel(model , X_train_Singleton_Pairwise , y_train,verboze, batchSize):
# Compile model
   model.compile(loss='mse', optimizer='rmsprop', metrics=['accuracy'])
   #model.summary()
   sigl= X_train_Singleton_Pairwise[0].reshape(1,X_train_Singleton_Pairwise[0].shape[0],X_train_Singleton_Pairwise[0].shape[1])
   pair = X_train_Singleton_Pairwise[1].reshape(1,X_train_Singleton_Pairwise[1].shape[0],X_train_Singleton_Pairwise[1].shape[1],X_train_Singleton_Pairwise[1].shape[2])
   model.fit([sigl,pair], y_train, epochs=10, batch_size = batchSize ,validation_split = 0.2, verbose=verboze)
   return model

def compile_fit_TwoInputModel(model , input1 , input2 ,output,verboze,epochsNum):
    sgd = keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(loss='categorical_crossentropy', optimizer= adam, metrics=['accuracy'])
    history = model.fit([input1 , input2], output, batch = input1.shape[0],epochs=epochsNum ,validation_split = 0.2, verbose=verboze)
    return model , history

def compile_fit_TwoInputModel(model, input1, input2, output, verboze,epochsNum, loss):
   # sgd = keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)#, amsgrad=False)
    model.compile(loss=loss, optimizer= adam, metrics=['accuracy'])
    history = model.fit([input1 , input2], output, batch_size = input1.shape[0],epochs=epochsNum ,validation_split = 0.2, verbose=verboze , callbacks =[tensorboard])# , trace.attention()])
    return model , history

def getAttentionForSingleAgent( singleton_sample, pairwise_sample,model):
    singleton_sample = np.expand_dims((singleton_sample), axis=0)
    pairwise_sample = np.expand_dims((pairwise_sample), axis=0)
    attention_vector = au.get_activations_simple(model,singleton_sample,pairwise_sample, print_shape_only=True,layer_name='attention_vec')
    attention_vector= attention_vector[0].flatten()
    return attention_vector
def getAttentionForAgent( singleton_sample, pairwise_sample,model):
    #singleton_sample = np.expand_dims((singleton_sample), axis=0)
    #pairwise_sample = np.expand_dims((pairwise_sample), axis=0)
    attention_vector = au.get_activations_simple(model,singleton_sample,pairwise_sample, print_shape_only=True,layer_name='attention_vec')
    attention_vector= attention_vector[0].flatten()
    return attention_vector
# evaluate the model
#scores = model.evaluate(X, Y)
def evaluateModel(model,X_test,Y_test):
   testAcc = model.evaluate(X_test,Y_test)
   print(testAcc)

def plotModel(model , fileName):
    from keras.utils import plot_model
    plot_model(model, to_file=fileName)

#%% Start Loading the syntheticData
def generateSynthetic(agents,features):
    import utils as ut
    syntheticData = loadMultiVariateDist(agents , features)
    return syntheticData
    #scalling all value to be withing the range 0-1 variance 1 mean 0
def scaleAndTransform(syntheticData):
    scaler = StandardScaler()
    scaler.fit(syntheticData)
    X_scaled = scaler.transform(syntheticData)
    X=X_scaled # X now has the synthetic data
    #keep the firt two PC of the data
    return X

def doPCA(X ,dims):
    pca = PCA(n_components=dims)
    #fit PCA model to data
    pca.fit(X)
    X_pca = pca.transform(X) #X_pca_2d has the 2D Data
    return X_pca
def saveModel(model , name):
    name = name+".h5"
    model.save(name)
#%%
#Do clustering
#mglearn.plots.plot_kmeans_algorithm()
def Kmeans(X,num_clusters):
    from sklearn.datasets import make_blobs
    from sklearn.cluster import KMeans
    #Clustering using Kmeans with the scaled raw data X
    kmeans = KMeans(n_clusters=num_clusters)
    kmeans.fit(X)
    print("Cluster memberships:\n{}".format(kmeans.labels_))
    if(X.shape[1]==2):
        mglearn.discrete_scatter(X[:,0], X[:,1],kmeans.labels_ , markers ='o')
        #.cluster_centers_
    return kmeans
#%%
#one-hot encoding
def oneHotEncoding(clstrLabels):
    clusterLabels = pd.DataFrame({'Clusters': clstrLabels})
   # display(clusterLabels)
    # need to explicitly mention the name of the column because pandas will consider all
    ##numbers continous data and will not produce one-hot encoding for them
  #  display(pd.get_dummies(clusterLabels,columns =['Clusters']))
    oneHotEncodedLabels = (pd.get_dummies(clusterLabels,columns =['Clusters']))
  #  display(oneHotEncodedLabels)
    print(clusterLabels['Clusters'].value_counts())
    return oneHotEncodedLabels

def getClusteredData(numfeatures,numAgents= 200 ,Plot = False ):
    import math
    dlen = math.floor(numAgents/4)
    numfeatures = 2
    Data1 = pd.DataFrame(data= 1*np.random.rand(dlen,numfeatures))
    Data1.values[:,1] = (Data1.values[:,0][:,np.newaxis] + .42*np.random.rand(dlen,1))[:,0]


    Data2 = pd.DataFrame(data= 1*np.random.rand(dlen,numfeatures)+1)
    Data2.values[:,1] = (-1*Data2.values[:,0][:,np.newaxis] + .62*np.random.rand(dlen,1))[:,0]

    Data3 = pd.DataFrame(data= 1*np.random.rand(dlen,numfeatures)+2)
    Data3.values[:,1] = (.5*Data3.values[:,0][:,np.newaxis] + 1*np.random.rand(dlen,1))[:,0]


    Data4 = pd.DataFrame(data= 1*np.random.rand(dlen,numfeatures)+3.5)
    Data4.values[:,1] = (-.1*Data4.values[:,0][:,np.newaxis] + .5*np.random.rand(dlen,1))[:,0]


    DataCL1 = np.concatenate((Data1,Data2,Data3,Data4))

    if Plot :
        fig = plt.figure()
        plt.plot(DataCL1[:,0],DataCL1[:,1],'ob',alpha=0.2, markersize=4)
        fig.set_size_inches(5,5)

    return DataCL1

#getClusteredData(2 , 2000)
earlyStopping = keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0.000001, patience=10, verbose=0, mode='auto')
