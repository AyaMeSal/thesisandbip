# -*- coding: utf-8 -*-
"""
Created on Fri May 25 12:31:54 2018

@author: Aya
"""
import utils as ut
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
import numpy as np
#%%
numEpochs = 1

# 100 cells 
# 5 genes
hidden_dim = 100
run = 1
numSamples = 50 # number of cells
numFeatures = 8 # number of genes
listi = [i%2 for i in range (1,numFeatures*numFeatures+1)]
cov= np.array(listi)
cov=cov.reshape(numFeatures, numFeatures)   
mu = np.zeros(numFeatures)

def getData(numSets):
    numSamples = 50 # number of cells
    numFeatures = 6 # number of genes
    listi = [i%2 for i in range (1,numFeatures*numFeatures+1)]
    cov = np.array(listi)
    cov = cov.reshape(numFeatures, numFeatures)  
    #cov = cov *2
    #cov[0][4]=3 ;  
    #cov[4][0]=3;

    mu = np.zeros(numFeatures)
    #cov = np.eye(5)
   
    #cov2=np.eye(numFeatures)
    #%%
    #%%
    """
    geneExpression = np.random.multivariate_normal(mu,cov, size= numSamples)
    arr = geneExpression
    n=0
    #%%"
    
    scaler = MinMaxScaler()
    scaler.fit(geneExpression)
    geneExpression = scaler.transform(geneExpression)
    """
    scaler = MinMaxScaler()
    
    #%%
    singData=[]
    pairData=[]
    lablsData =[]
    singleton_train =[] ;
    pairwise_train =[] ;
    labels_train=[]
    
    for i in range(1,numSets):
        geneExpression = np.random.multivariate_normal(mu,cov, size= numSamples)
        geneExpression = geneExpression.T #flipped genes are now corsw and cells colsss
        scaler.fit(geneExpression)
        geneExpression = scaler.transform(geneExpression)
        geneExpInput = geneExpression
        #geneExpInput= np.expand_dims((geneExpInput), axis=1)
        lbls = geneExpression
        #lbls= np.expand_dims((geneExpOutput), axis=1)
        #X_train, X_test, y_train, y_test = ut.train_test_split(geneExpInput,lbls, 0,  shuffle = False)
        singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data_self(geneExpInput,lbls)
        singData.append(singleton_train)
        pairData.append(pairwise_train)
        lablsData.append(labels_train)
		
    if(numSets>1):
	
        singleton_train = np.vstack(x for x in singData)
        pairwise_train = np.vstack(x for x in pairData)
        labels_train = np.vstack(x for x in lablsData)
		
    return singleton_train , pairwise_train, labels_train

#singData.append(singleton_train)
#pairData.append(pairwise_train)
#%%
"""
geneExpression = np.random.multivariate_normal(mu,cov, size= numSamples)
geneExpInput = geneExpression.T
#geneExpInput= np.expand_dims((geneExpInput), axis=1)
lbls = geneExpression.T
#lbls= np.expand_dims((geneExpOutput), axis=1)

X_train, X_test, y_train, y_test = ut.train_test_split(geneExpInput,lbls, 0)
singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data_self(X_train , y_train)
singleton_test , pairwise_test , labels_test ,pairwiseLabels_test = ut.get_data_self(X_test , y_test)
"""
if __name__ == "__main__":
 
    singleton_train , pairwise_train , lbls = getData(500)
    import ThesisModel_AttentionMatrix as tm
    from keras import metrics
    import keras
    #adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)#, amsgrad=False)
    
    GeneAutoEncoder  = tm.get_full_model_reg(hidden_dim,singleton_train.shape[1],numFeatures,lbls.shape[1])
    #VAIN_Evolution.compile(loss='mean_squared_error', optimizer= adam, metrics=[metrics.mae])
    GeneAutoEncoder.compile(loss='binary_crossentropy', optimizer= 'adadelta', metrics=[metrics.mae])
    
    #hidden_size,features, numagents,labels_shape
    
    epochcount = 1
    try:
        while True :  #, batch_size = 5
            GeneAutoEncoder_History = GeneAutoEncoder.fit([singleton_train , pairwise_train], singleton_train,epochs=numEpochs ,verbose=1)# ,validation_data =([singleton_train , pairwise_train], labels_train), # , callbacks =[tensorboard])# , trace.attention()])
            print("Epoch number %d" %epochcount)
            epochcount= epochcount+1
    except KeyboardInterrupt:
    	pass
    
    ut.saveModel(GeneAutoEncoder , "GeneAutoEncoder_Synthdata_ChekCov_28May"+str(epochcount)+"_hidden"+str(hidden_dim)+"_feat"+ str(numFeatures))
        
    """
    model = AttentionSeq2Seq(input_dim=geneExpInput.shape[2], input_length=geneExpInput.shape[1], hidden_dim=10, output_length=geneExpOutput.shape[1], output_dim=geneExpOutput.shape[2], depth=4)
    model.compile(loss='mse', optimizer='adam')
    history = model.fit(geneExpInput, geneExpOutput, epochs=numEpochs , verbose=2)
    testSample = np.array(geneExpInput[1]).reshape(1,1,geneExpInput.shape[2])
    prediction = geneExpOutput[1]
    print(prediction)
    model.predict(testSample)#,geneExpOutput[1])
    """
