# -*- coding: utf-8 -*-
"""
Created on Fri May 25 16:04:09 2018

@author: Aya
"""
import utils as ut
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import syntheticDataExperiment as synt

#%%
numEpochs = 1

# 100 cells 
# 5 genes
"""
numSamples = 500 # number of cells
numFeatures = 400 # number of gene
listi = [i%2 for i in range (1,numFeatures*numFeatures+1)]
cov= np.array(listi)
cov=cov.reshape(numFeatures, numFeatures)    
cov2=np.eye(numFeatures)
#%%
#%%
mu = np.zeros(numFeatures)
geneExpression = np.random.multivariate_normal(mu,cov, size= numSamples)
geneExpression=geneExpression.T


scaler = MinMaxScaler()
scaler.fit(geneExpression)
geneExpression = scaler.transform(geneExpression)
#%%
geneExpInput = cov
#geneExpInput= np.expand_dims((geneExpInput), axis=1)
lbls = cov
#lbls= np.expand_dims((geneExpOutput), axis=1)

X_train, X_test, y_train, y_test = ut.train_test_split(geneExpInput,lbls, 0)
singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data_self(X_train , y_train)
singleton_test , pairwise_test , labels_test ,pairwiseLabels_test = ut.get_data_self(X_test , y_test)
"""
#%%
singleton_train , pairwise_train , labels_train = synt.getData(2)
lbls = singleton_train
#geneExpInput, X_test, geneExpOutput, y_test = ut.train_test_split(geneExpInput,geneExpOutput, 0.2)
import ThesisModel_AttentionMatrix as tm
from keras import metrics
#import ThesisModel as t
#adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)#, amsgrad=False)
GeneAutoEncoder  = tm.get_full_model_reg(synt.hidden_dim,singleton_train.shape[1],singleton_train.shape[0],lbls.shape[1])

GeneAutoEncoder.load_weights("GeneAutoEncoder_Synthdata_ChekCov_28May56_hidden100_feat8.h5")
#checkpointer = ModelCheckpoint(filepath="/tmp/weights.hdf5", verbose=1, save_best_only=True)
#%%
"""
fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(8, 8))
attention=[]
attention.append([])
attention.append([])
attention.append([])
attention.append([])
attention.append([])

for col_indx in range(0, labels_train.shape[1]):
     #make mask from clusterLabels
     hist = [0,0,0,0,0]
     all_col = labels_train[:,col_indx]
     cluster_mask = all_col > 0
     singletonFeaturesForClusterX = singleton_train[cluster_mask]
     pairwiseFeaturesForClusterX = pairwise_train[cluster_mask]
     LablesforOtherAgents = pairwiseLabels_train[cluster_mask]
     histForClusterX = [0,0,0,0,0]
     index = 0
     for classMember in singletonFeaturesForClusterX:
         singleton = classMember
         pairwise = pairwiseFeaturesForClusterX[index]
         othersLabels = LablesforOtherAgents[index]
         attention_vector = ut.getAttentionForSingleAgent(singleton , pairwise,GeneAutoEncoder)#cellPCA_model)
         
         attention[col_indx].append(attention_vector)
         histForClusterX += ut.getHistForAttentionVector(attention_vector , othersLabels)
         index+=1
    #plot the graph for this agent
    #x-axis all agents lables
    #y-axis the count of the number of times they were given attention
     indx0 = int(col_indx/2)
     indx1 = int(col_indx%2)

     pd.DataFrame(histForClusterX, columns=['attention count']).plot(ax=axes[indx0,indx1],kind='bar',title='Attention Mechanism '+' for cluster'+str(col_indx))

#%%
     
origsing = singleton_train[:5 , :]
origPair = pairwise_train[:5 , :]

#%%
singleton_train = origsing
pairwise_train = origPair
"""
#%%
attention2=[]
fig,ax = plt.subplots(figsize=(30, 30))
meanAttention = np.zeros(singleton_train.shape[0])
#arr = singleton_train
#arr_newOrder = arr[:,0].argsort()
#arr = arr[arr_newOrder]
#pairwise_train = pairwise_train[arr_newOrder]
for i in range (0,1000):
    singleton_train , pairwise_train , labels_train = synt.getData(2)
    attention2=[]
    for indx in range (len(singleton_train)):
         singleton = singleton_train[indx]
         pairwise = pairwise_train[indx]
         attention_vector = ut.getAttentionForSingleAgent(singleton , pairwise,GeneAutoEncoder)#cellPCA_model)
         attention2.append(attention_vector)
    
    attention2 = np.vstack(x for x in attention2)
    meanAttention = meanAttention+ attention2


avgatt = np.mean(attention2 , axis =1)

#%%
meanAttention = meanAttention/1000
fig, ax = plt.subplots(figsize=(30, 30))
ax.imshow(meanAttention,aspect='auto')
plt.show()


   
#%%    
fig, ax = plt.subplots(figsize=(20, 20))
fig, ax2 = plt.subplots(figsize=(20, 20))

attenionordered = np.vstack(x for x in attention)

attention2 = np.vstack(x for x in attention2)


ax.imshow(attention2,aspect='auto')
ax2.imshow(attenionordered,aspect='auto')
plt.show()
#plot the graph for this agent
#x-axis all agents lables
#y-axis the count of the number of times they were given attention
