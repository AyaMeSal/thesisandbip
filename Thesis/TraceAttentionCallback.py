# -*- coding: utf-8 -*-
"""
Created on Mon May 21 00:54:20 2018

@author: Aya
"""
import keras
from sklearn.metrics import roc_auc_score

class attention(keras.callbacks.Callback):

	def on_train_begin(self, logs={}):
		self.aucs = []
		self.losses = []

	def on_train_end(self, logs={}):
		return

	def on_epoch_begin(self, epoch, logs={}):
		return

	def on_epoch_end(self, epoch, logs={}):
		self.losses.append(logs.get('loss'))
		y_pred = self.model.predict(self.validation_data[0])
		self.aucs.append(roc_auc_score(self.validation_data[1], y_pred))
		return

	def on_batch_begin(self, batch, logs={}):
		return

	def on_batch_end(self, batch, logs={}):
		for layer in self.model.layers:
            if layer.name == 'attention_vec':
                outputs = layer.output
                print(outputs)



