# -*- coding: utf-8 -*-
"""
Created on Sat May 19 00:44:14 2018

@author: Aya
"""
#%%
# Feature Extraction with RFE
from pandas import read_csv
import utils as ut
# load data
url = "https://raw.githubusercontent.com/jbrownlee/Datasets/master/pima-indians-diabetes.data.csv"
names = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi', 'age', 'class']
dataframe = read_csv(url, names=names)
array = dataframe.values
X = array[:,0:8]
Y = array[:,8]
# feature extraction
ut.SelectFeaturesRFE(X , Y)