# -*- coding: utf-8 -*-
"""
Created on Fri May 18 01:06:43 2018

@author: Aya
"""
import numpy as np
import seq2seq
from seq2seq.models import AttentionSeq2Seq


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 15:05:32 2018

@author: a
"""
#get the data
#split up the data
#get the model
# run the model
import utils as ut
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
from sklearn.decomposition import PCA
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
import numpy as np
#%%
numEpochs = 1000
#%%
geneExpFile="C:/Data/GE_mvg.csv"
annotationFile="C:/Data/Annotation.csv"
df = read_csv(geneExpFile)# ,header = None)
dfStd = df.std()
dfMean = dfStd.mean()
df=df.loc[:, df.std() > (0.5*dfMean)]

geneExpression = np.array(df)
Annotations= read_csv(annotationFile)
pseudoTime = Annotations['Pseudotime']
df = df.assign(pseudoTime = pseudoTime.values )
df.sort_values("pseudoTime", inplace=True)


#%%
combinedDFasArray = np.array(df)
combinedDFasArray = combinedDFasArray[:,:-1]

variances = (np.std(combinedDFasArray ,axis =0))
df.sort_values(variances.argsort(), axis=1)
#keep only highest 500 std

scaler = MinMaxScaler()
scaler.fit(combinedDFasArray)
combinedDFasArray = scaler.transform(combinedDFasArray)
#%%Do PCA
dims = 500
pca = PCA(n_components=dims)
#fit PCA model to data
pca.fit(combinedDFasArray)
combinedDFasArray = pca.transform(combinedDFasArray)
#%%
geneExpInput = combinedDFasArray[:-1 ,:]
geneExpInput= np.expand_dims((geneExpInput), axis=1)
geneExpOutput = combinedDFasArray[1: ,:]
geneExpOutput= np.expand_dims((geneExpOutput), axis=1)

#geneExpInput, X_test, geneExpOutput, y_test = ut.train_test_split(geneExpInput,geneExpOutput, 0.2)

model = AttentionSeq2Seq(input_dim=geneExpInput.shape[2], input_length=geneExpInput.shape[1], hidden_dim=10, output_length=geneExpOutput.shape[1], output_dim=geneExpOutput.shape[2], depth=4)
model.compile(loss='mse', optimizer='adam')
history = model.fit(geneExpInput, geneExpOutput, epochs=numEpochs , verbose=2)
testSample = np.array(geneExpInput[1]).reshape(1,1,geneExpInput.shape[2])
prediction = geneExpOutput[1]
print(prediction)
model.predict(testSample)#,geneExpOutput[1])


