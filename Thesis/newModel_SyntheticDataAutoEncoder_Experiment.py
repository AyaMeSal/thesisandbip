# -*- coding: utf-8 -*-
"""
Created on Wed May 30 19:41:07 2018

@author: Aya
"""

import utils as ut
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
import numpy as np
import syntheticDataExperiment as synt

#%%

# 100 cells 
# 5 genes
run = 1

hidden_size = 100
epochcount= 500 
numSamples = 100 # number of cells
numFeatures = 5 # number o
weightsFileName = "newModel_syntheticDataAutoEncoder_Experiment_2_6_checkerCov_"+str(numFeatures)+"_"+str(epochcount)+"_"+str(hidden_size)+".h5"

#%%
#%%
def getData(numSets):

    listi = [i%2 for i in range (1,numFeatures*numFeatures+1)]
    cov = np.array(listi)
    #cov = np.eye(numFeatures)
    #allOnes = np.ones(numFeatures*numFeatures)
    cov = cov.reshape(numFeatures, numFeatures)
    #cov = allOnes+cov
    
    #cov = cov *2
    # cov[0][4]=3 ;  
    #cov[4][0]=3;
    mu = np.zeros(numFeatures)
    scaler = MinMaxScaler()
    singData=[]
    for i in range(1,numSets):
        geneExpression = np.random.multivariate_normal(mu,cov, size= numSamples)
        geneExpression = geneExpression.T #flipped genes are now corsw and cells colsss
        scaler.fit(geneExpression)
        geneExpression = scaler.transform(geneExpression)
        geneExpInput = geneExpression
        #geneExpInput= np.expand_dims((geneExpInput), axis=1)
        #lbls= np.expand_dims((geneExpOutput), axis=1)
        #X_train, X_test, y_train, y_test = ut.train_test_split(geneExpInput,lbls, 0,  shuffle = False)
        singData.append(geneExpInput)
		
    return singData

#%%


if __name__ == "__main__":
    
    data = np.array(getData(500))

  	#lbls= np.expand_dims((geneExpOutput), axis=1)

	#X_train, X_test, y_train, y_test = ut.train_test_split(geneExpInput,lbls, 0)
	#singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data_self(X_train , y_train)
	#singleton_test , pairwise_test , labels_test ,pairwiseLabels_test = ut.get_data_self(X_test , y_test)
    # labels_train = singleton_train
    X_train, X_test, y_train, y_test = ut.train_test_split(data,data, 0,  shuffle = False)
	#geneExpInput, X_test, geneExpOutput, y_test = ut.train_test_split(geneExpInput,geneExpOutput, 0.2)
    import ThesisModel_AttentionMatrix as tm
    from keras import metrics
	#import ThesisModel as t
    import keras
	#adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)#, amsgrad=False)
    GeneAutoEncoder  = tm.get_full_model_1Input_reg(hidden_dim ,X_train.shape[2], X_train.shape[1], y_train.shape[2])
	#VAIN_Evolution.compile(loss='mean_squared_error', optimizer= adam, metrics=[metrics.mae])
    GeneAutoEncoder.compile(loss='mse', optimizer= 'adadelta', metrics=[metrics.mae])
    saveBest = keras.callbacks.ModelCheckpoint(weightsFileName, monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)
	#try:
	# while True:
    GeneAutoEncoder_History = GeneAutoEncoder.fit(singleton_train, labels_train, batch_size=32 ,epochs=epochcount ,validation_data =(X_test , y_test), verbose=2, callbacks =[saveBest])# , trace.attention()])
    print("Epoch number %d" %epochcount)
    epochcount= epochcount+1





