import utils as ut
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
from sklearn.decomposition import PCA
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
import numpy as np
import matplotlib.pyplot as plt
import os
import newModel_Thrombocytes_gene_AutoEncoder_Experiment as model
#%%
numEpochs = 1
geneExpression = model.getData()
#%%
geneExpInput = geneExpression.T
#geneExpInput= np.expand_dims((geneExpInput), axis=1)
lbls = geneExpression.T
#lbls= np.expand_dims((geneExpOutput), axis=1)

#X_train, X_test, y_train, y_test = ut.train_test_split(geneExpInput,lbls, 0)
#singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data_self(X_train , y_train)
#singleton_test , pairwise_test , labels_test ,pairwiseLabels_test = ut.get_data_self(X_test , y_test)
singleton_train = geneExpInput
labels_train = geneExpInput

#geneExpInput, X_test, geneExpOutput, y_test = ut.train_test_split(geneExpInput,geneExpOutput, 0.2)
import ThesisModel_AttentionMatrix as tm

#adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)#, amsgrad=False)
#	pass
from keras import backend as K
K.set_learning_phase(0)
GeneAutoEncoder_loadWeight  = tm.get_full_model_1Input_reg(model.hidden_size,singleton_train.shape[1],singleton_train.shape[0],labels_train.shape[1])

#load weights for bect model
#VAIN_Clustering.load_weights("VAIN_Clustering1.h5")
GeneAutoEncoder_loadWeight.load_weights(model.weightsFileName)
attention = ut.getAttentionForAgentSingleInput(geneExpInput,GeneAutoEncoder_loadWeight)
attantion = np.array(attention)
print(attention.shape)
attention=attention.reshape(singleton_train.shape[0],singleton_train.shape[0])

print(attention.shape)
scaler = MinMaxScaler()
#scaler.fit(attention)
#attention = scaler.transform(attention)

#%% get the most related genes to gene 1
import pandas as pd
df = read_csv('./Data/CV_genes.csv')# ,header = None)
geneList = np.array(df)
cutoff_percentage = [0.5 ,0.7, 0.9]
for cutoff in cutoff_percentage:
    gene_index = 0
    #list of related genes should be as long as the genes present
    most_attended_genes = []
    for gene in geneList:
        #pull out the first row 
        attention_per_gene = attention[gene_index,:]
        attention_per_gene = attention_per_gene.reshape(-1,1)
        scaler.fit(attention_per_gene)
        attention_per_gene = scaler.transform(attention_per_gene)
        most_related_genes_mask = np.array(attention_per_gene>cutoff)
        significnt_genes_gene1 = geneList[most_related_genes_mask]
        most_attended_genes.append(significnt_genes_gene1)
        gene_index = gene_index+1
        geneList = np.array(df)
    
    related_genes_df = pd.DataFrame(most_attended_genes)
    related_genes_df.to_csv("./Data/finalResults/"+model.cellType+"_GeneConnections_cutoffpercentage"+str(cutoff*100)+".csv")

#%%
#make cutoff mask based on a percentage 
#get the related genes based on those genes

attn_df = pd.DataFrame(most_attended_genes)
attn_df.to_csv(model.weightsFileName+"_AttentionMatrix.csv")


from PIL import Image
im = Image.fromarray(attention*255,mode ='L')
im.save(model.weightsFileName+"_AttentionMatrixImage.jpeg")

"""
fig, ax = plt.subplots(figsize=(30, 30))
ax.imshow(attention,aspect='auto')
plt.show()
"""

#ut.saveModel(GeneAutoEncoder , "./NewModelLogs/NewModel_GeneAutoEncoderRealData"+str(numEpochs))

"""
model = AttentionSeq2Seq(input_dim=geneExpInput.shape[2], input_length=geneExpInput.shape[1], hidden_dim=10, output_length=geneExpOutput.shape[1], output_dim=geneExpOutput.shape[2], depth=4)
model.compile(loss='mse', optimizer='adam')
history = model.fit(geneExpInput, geneExpOutput, epochs=numEpochs , verbose=2)
testSample = np.array(geneExpInput[1]).reshape(1,1,geneExpInput.shape[2])
prediction = geneExpOutput[1]
print(prediction)
model.predict(testSample)#,geneExpOutput[1])
"""
