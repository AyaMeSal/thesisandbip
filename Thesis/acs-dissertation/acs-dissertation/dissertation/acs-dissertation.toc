\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {chapter}{\numberline {2}Background}{3}
\contentsline {subsection}{\numberline {2.0.1}The DataSet: RNA-Seq Data}{3}
\contentsline {subsection}{\numberline {2.0.2}GeneOntology}{3}
\contentsline {subsection}{\numberline {2.0.3}GSEA: Gene Set enrichment and depletion analysis}{5}
\contentsline {subsection}{\numberline {2.0.4}Conducting GSEA}{6}
\contentsline {chapter}{\numberline {3}Related Work}{7}
\contentsline {chapter}{\numberline {4}Design and Implementation}{9}
\contentsline {section}{\numberline {4.1}clustering}{9}
\contentsline {section}{\numberline {4.2}Multi-layer perceptron}{9}
\contentsline {section}{\numberline {4.3}Attention}{9}
\contentsline {section}{\numberline {4.4}residual connection}{9}
\contentsline {chapter}{\numberline {5}Evaluation}{11}
\contentsline {chapter}{\numberline {6}Summary and Conclusions}{13}
