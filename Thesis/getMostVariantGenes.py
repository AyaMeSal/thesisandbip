# -*- coding: utf-8 -*-
"""
Created on Sat May 19 13:29:34 2018

@author: Aya
"""
# -*- coding: utf-8 -*-
"""
Created on Sat May 19 01:05:05 2018
This experiment test the visualilation of the attention learned When the most variable genes are used as
input to an VAIN Model
@author: Aya
"""

import numpy as np
import pandas as pd
from pandas import read_csv
import utils as ut

#%%
numEpochs = 1
#%%

geneExpFile="C:/Data/GE_mvg.csv"
geneNamesFile="C:/Data/CV_genes.csv"
geneNamesDataframe = read_csv(geneNamesFile ,dtype={"gene": str})
geneNames =  list(geneNamesDataframe['gene'])
df = read_csv(geneExpFile, names = geneNames)# ,header = None)
dfStd = df.std()
dfMean = dfStd.mean()
df=df.loc[:, df.std() > 0.5*(dfMean)]
ut.isNullData(df)
df.to_csv('./700_genes.csv')


"""
annotationFile="C:/Data/Annotation.csv"

Annotations= read_csv(annotationFile)
clusterLabels = Annotations['State']
lbls = ut.oneHotEncoding(clusterLabels)

lbls.to_csv('./Cluster_Lables.csv')
"""