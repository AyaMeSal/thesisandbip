import utils as ut
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
from sklearn.decomposition import PCA
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import tensorflow as tf
import random as rn


df = read_csv('./274_genes.csv')# ,header = None)
lbls = read_csv('./Cluster_Lables.csv')

geneExpression = np.array(df)
geneExpression = geneExpression[: ,1:]
#%%
scaler = MinMaxScaler()
scaler.fit(geneExpression)
X = scaler.transform(geneExpression)
#one-hot Encoding:
lbls = np.array(lbls)
lbls=lbls[:,1:]
#%% VAIN Model
X_train, X_test, y_train, y_test = ut.train_test_split(X,lbls, 0.5)
singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data_self(X_train , y_train)
singleton_test , pairwise_test , labels_test ,pairwiseLabels_test = ut.get_data_self(X_test , y_test)

import ThesisModel_AttentionMatrix as tm
#import ThesisModel as t
VAIN_Clustering  = tm.get_full_model(singleton_train.shape[1],singleton_train.shape[0],lbls.shape[1])

VAIN_Clustering.load_weights("VAIN_Clustering1.h5")
#checkpointer = ModelCheckpoint(filepath="/tmp/weights.hdf5", verbose=1, save_best_only=True)
#%%
fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(8, 8))
attention=[]
attention.append([])
attention.append([])
attention.append([])
attention.append([])
attention.append([])

for col_indx in range(0, labels_train.shape[1]):
     #make mask from clusterLabels
     hist = [0,0,0,0,0]
     all_col = labels_train[:,col_indx]
     cluster_mask = all_col > 0
     singletonFeaturesForClusterX = singleton_train[cluster_mask]
     pairwiseFeaturesForClusterX = pairwise_train[cluster_mask]
     LablesforOtherAgents = pairwiseLabels_train[cluster_mask]
     histForClusterX = [0,0,0,0,0]
     index = 0
     for classMember in singletonFeaturesForClusterX:
         singleton = classMember
         pairwise = pairwiseFeaturesForClusterX[index]
         othersLabels = LablesforOtherAgents[index]
         attention_vector = ut.getAttentionForSingleAgent(singleton , pairwise,VAIN_Clustering)#cellPCA_model)
         
         attention[col_indx].append(attention_vector)
         histForClusterX += ut.getHistForAttentionVector(attention_vector , othersLabels)
         index+=1
    #plot the graph for this agent
    #x-axis all agents lables
    #y-axis the count of the number of times they were given attention
     indx0 = int(col_indx/2)
     indx1 = int(col_indx%2)

     pd.DataFrame(histForClusterX, columns=['attention count']).plot(ax=axes[indx0,indx1],kind='bar',title='Attention Mechanism '+' for cluster'+str(col_indx))

#%%
attention2=[]
for indx in range (len(singleton_train)):
     singleton = singleton_train[indx]
     pairwise = pairwise_train[indx]
     attention_vector = ut.getAttentionForSingleAgent(singleton , pairwise,VAIN_Clustering)#cellPCA_model)
     attention2.append(attention_vector)
   
#%%    
fig, ax = plt.subplots(figsize=(20, 20))
fig, ax2 = plt.subplots(figsize=(20, 20))

attenionordered = np.vstack(x for x in attention)

attention2 = np.vstack(x for x in attention2)
attention2=attention2*5000


ax.imshow(attention2,aspect='auto')
ax2.imshow(attenionordered,aspect='auto')
plt.show()
#plot the graph for this agent
#x-axis all agents lables
#y-axis the count of the number of times they were given attention
