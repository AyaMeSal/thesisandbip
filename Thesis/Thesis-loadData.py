# -*- coding: utf-8 -*-
#%%
"""
Created on Tue Mar 20 12:12:18 2018

@author: Aya
"""
from pandas import read_csv
import pandas as pd
import numpy as np
from sklearn.datasets import load_breast_cancer
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import mglearn
from sklearn.model_selection import train_test_split
import utils as ut
#%%
"""
RatingsFile = "C:/Data/GE_mvg.csv"
df = read_csv(RatingsFile)
geneExpression = np.array(df)
"""
#%%This block deal with canver data from sci-kit learn
"""
cancer = load_breast_cancer() #cancer
scaler = StandardScaler() #scaler to trasfor 
scaler.fit(cancer.data) # cancer again
X_scaled = scaler.transform(cancer.data) #trasform cancer data
#keep the firt two PC of the data
pca = PCA(n_components=2) #create the PCS instances , do  2 comp PCA
#fit PCA model to cancer data
pca.fit(X_scaled)  # canmcer data

X_pca = pca.transform(X_scaled) #cancer data

print("Original shape: {}".format (str(X_scaled.shape))) #PCA Shapes of cancer data
print("Reduced shape: {}".format (str(X_pca.shape))) #PCA Shapes

#plot first Vs second principal component colored by class
plt.figure(figsize=(8,8)) #plotting the graphs
mglearn.discrete_scatter(X_pca[:,0],X_pca[:,1],cancer.target)
plt.legend(cancer.target_names, loc="best")
plt.gca().set_aspect("equal")
plt.xlabel("First principal component")
plt.ylabel("Second principal component")
"""
#%%
"""
RatingsFile = "C:/Data/GE_mvg.csv"
df = read_csv(RatingsFile)
geneExpression = np.array(df)
geneExpressionNoIndex = geneExpression[:,1:]
"""
#%% Statt Loading the syntheticData

numAgents = ut.numAgents
syntheticData = ut.loadMultiVariateDist(numAgents , 10)
#scalling all value to be withing the range 0-1 variance 1 mean 0
scaler = StandardScaler()
scaler.fit(syntheticData)
X_scaled = scaler.transform(syntheticData)
X=X_scaled # X now has the synthetic data
#keep the firt two PC of the data
dims = 2
pca = PCA(n_components=dims)
#fit PCA model to data
pca.fit(X)
X_pca_2d = pca.transform(X) #X_pca_2d has the 2D Data
#%% 5D
dims = 5
pca = PCA(n_components=dims)
#fit PCA model to data
pca.fit(X)
X_pca_5d = pca.transform(X) #X_pca_5d has the 5D Data
#%%
X_pca = X_pca_2d #X_pca now has either 2D or 5D
print("Original shape: {}".format (str(X)))
print("Reduced shape: {}".format(str(X_pca.shape)))
#%%
#plot first Vs second principal component colored by class
""" not sure what this is :
plt.figure(figsize=(8,8))
mglearn.discrete_scatter(X_pca[:,0],X_pca[:,1],geneExpression[:,1])
#plt.legend(cancer.target_names, loc="best")
plt.gca().set_aspect("equal")
plt.xlabel("First principal component")
plt.ylabel("Second principal component")
"""
#%%
#mglearn.plots.plot_kmeans_algorithm()
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
#Clustering using Kmeans with the scaled raw data X
X = ut.getClusteredData(2 , 2000)

kmeans = KMeans(n_clusters=4)
kmeans.fit(X)
print("Cluster memberships:\n{}".format(kmeans.labels_))
mglearn.discrete_scatter(X[:,0], X[:,1],kmeans.labels_ , markers ='o')

mglearn.discrete_scatter(X_pca[:,0], X_pca[:,1],kmeans.labels_ , markers ='o')
#.cluster_centers_ 
#mglearn.discrete_scatter(kmeans.cluster_centers_[:,0],kmeans.cluster_centers_[:,1], [0,1,2],markers ='^',markeredgewidth=2)
#Step1: Preprocessing /scalling ?
#Step2 :dimensionality reduction
#Step3:Clustering

#%% Ineraction with 5D data
#naive interactions with combining features 
from sklearn.preprocessing import PolynomialFeatures
poly = PolynomialFeatures(interaction_only=True,include_bias = False)
interactionData = poly.fit_transform(X_pca_5d)
#Step4:Build MLP
#%%
X = X_pca_5d
clstrLabels = kmeans.labels_;
#one-hot encoding
clusterLabels = pd.DataFrame({'Clusters': clstrLabels})
display(clusterLabels)
# need to explicitly mention the name of the column because pandas will consider all
##numbers continous data and will not produce one-hot encoding for them
display(pd.get_dummies(clusterLabels,columns =['Clusters']))
oneHotEncodedLabels = (pd.get_dummies(clusterLabels,columns =['Clusters']))
display(oneHotEncodedLabels)
print(clusterLabels['Clusters'].value_counts())


#[0.11295001208782196, 0.5]


#%%
X_train, X_test, y_train, y_test = train_test_split(X,oneHotEncodedLabels, test_size=0.1)
#%% 2dInput
singleton , pairwise , labels = ut.get_data(X , oneHotEncodedLabels)
model = ut.build2DInputNetwork()#singleton_pairwise[1],y_train.shape[1])
model = ut.compileModel(model , singleton_pairwise[1] , oneHotEncodedLabels[1],2)
testAcc = model.evaluate(X_test,y_test)
print("2dInput Model")
print(model.metrics_names)
print(testAcc)
ut.plotModel(model,'test2dinput.png')
#%%complicated attention model
import ThesisModel as tm
pairwise.shape
singleton.shape
labels.shape
model = tm.get_full_model(singleton.shape[1],singleton.shape[0],labels.shape[1])
interactionModel =  ut.compile_fit_TwoInputModel(model, singleton, pairwise, labels,2)
testAcc = model.evaluate([singleton,pairwise],labels)

print("complex Attention Model")
print(model.metrics_names)
print(testAcc)
ut.plotModel(model,'model_full__attention.png')


#%% Attention Model
model = ut.build_attention_model(X_train.shape[1],y_train.shape[1])
model = ut.compileModel(model , X_train , y_train,2)
testAcc = model.evaluate(X_test,y_test)
print("Attention Model")
print(model.metrics_names)
print(testAcc)
ut.plotModel(model,'model_attention.png')
"""
[0.08012779355049134, 0.76]
"""
#%% Residual Connection 
model_res = ut.build_residual_connection_model(X_train.shape[1],y_train.shape[1])
model_res = ut.compileModel(model , X_train , y_train,2)
testAcc = model_res.evaluate(X_test,y_test)
print("res Model")
print(model.metrics_names)
print(testAcc)

#%%
model_MLP = ut.constructNeuralNet(X_train)
model_MLP = ut.compileModel(model , X_train , y_train,2)
testAcc = model_MLP.evaluate(X_test,y_test)
print("MLP Model")
print(model.metrics_names)
print(testAcc)
"""
[0.07914471387863159, 0.765]
"""
#%%InteractionNetwork :(
nb_classes = oneHotEncodedLabels.shape[1]
singleton_pairwise = ut.get_data(X , oneHotEncodedLabels.shape[1])
interactionModel = ut.constructInteractionNeuralNet((300,5),(300,299,10),5)#singleton_pairwise[0].shape, singleton_pairwise[1].shape,1)
interactionModel = ut.compileIntractioneModel(interactionModel , singleton_pairwise , oneHotEncodedLabels,1,10)

#%%
ut.plotModel(model,'model.png')
#%% density-based spatial clustering clustering of applications with noise ??
from sklearn.cluster import DBSCAN
dbscan = DBSCAN()
clusters = dbscan.fit_predict(X_pca)
plt.scatter(X_pca[:,0], X_pca[:,1], c= clusters , cmap = mglearn.cm2, s=60)
plt.xlabel("Feature 0")
plt.ylabel("Feature 1")

#%%Random Forest : P 86


