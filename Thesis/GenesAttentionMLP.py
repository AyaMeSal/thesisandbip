# -*- coding: utf-8 -*-
"""
Created on Fri May 11 11:40:55 2018

@author: Aya
"""
# load gene expression
import numpy as np
import utils as ut
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
import matplotlib.pyplot as plt
#%%
numEpochs = 10000
ut.fixSeeds()
#read Data
geneExpFile="C:/Data/GE_mvg.csv"
annotationFile="C:/Data/Annotation.csv"
df = read_csv(geneExpFile)
geneExpression = np.array(df)
Annotations= read_csv(annotationFile)
clusterLabels = Annotations['State']
#%%
scaler = MinMaxScaler()
scaler.fit(geneExpression)
X = scaler.transform(geneExpression)
#one-hot Encoding:
lbls = ut.oneHotEncoding(clusterLabels)
#%%
X_train, X_test, y_train, y_test = ut.train_test_split(X,lbls, 0.1)
#%% Attention Model
model = ut.build_attention_model(X_train.shape[1],y_train.shape[1])
model , hist_att = ut.compileModel(model , X_train , y_train,2 ,numEpochs)
testAcc_model = model.evaluate(X_test,y_test)
print("Attention Model")
print(model.metrics_names)
print(testAcc_model)
#%%
#%% AutoEncoder Model
autoEncoder = ut.constructAutoEncoder(X_train.shape[1],X_train.shape[1])
autoEncoder = ut.compileAutoEncoder(autoEncoder , X_train , X_train,0 ,numEpochs)
testAcc_autoEncoder = autoEncoder.evaluate(X_test,X_test)
print("autoencoder Model")
print(autoEncoder.metrics_names)
print(testAcc_autoEncoder)
#%%
model_MLP = ut.constructNeuralNet(X_train.shape[1],y_train.shape[1])
model_MLP , history = ut.compileModel(model_MLP , X_train , y_train,2,numEpochs)
testAcc_MLP = model_MLP.evaluate(X_test,y_test)
print("MLP Model")
print(model_MLP.metrics_names)
print(testAcc_MLP)
#%%
ut.plotHistory(hist_att ,"MLP with attention")
ut.plotHistory(history ,"MLP")
#%%Complicaed attention Model
