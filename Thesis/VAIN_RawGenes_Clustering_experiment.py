# -*- coding: utf-8 -*-
"""
Created on Sat May 19 13:29:34 2018

@author: Aya
"""
# -*- coding: utf-8 -*-
"""
Created on Sat May 19 01:05:05 2018
This experiment test the visualilation of the attention learned When the most variable genes are used as
input to an VAIN Model
@author: Aya
"""
#!/usr/bin/env python
import utils as ut
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
from sklearn.decomposition import PCA
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import tensorflow as tf
import random as rn

a = tf.constant([1.0 , 2.0 ,3.0 , 4.0 , 5.0 , 6.0], shape = [2,3], name = 'a')
b = tf.constant([1.0 , 2.0 ,3.0 , 4.0 , 5.0 , 6.0], shape = [3,2], name = 'b')
c = tf.matmul(a,b)

session_conf = tf.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)
sess = tf.Session(config=session_conf)
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)
print(sess.run(c))
#%%
# The below is necessary in Python 3.2.3 onwards to
# have reproducible behavior for certain hash-based operations.
# See these references for further details:
# https://docs.python.org/3.4/using/cmdline.html#envvar-PYTHONHASHSEED
# https://github.com/keras-team/keras/issues/2280#issuecomment-306959926

import os
os.environ['PYTHONHASHSEED'] = '0'

# The below is necessary for starting Numpy generated random numbers
# in a well-defined initial state.

np.random.seed(42)

# The below is necessary for starting core Python generated random numbers
# in a well-defined state.

rn.seed(12345)

# Force TensorFlow to use single thread.
# Multiple threads are a potential source of
# non-reproducible results.
# For further details, see: https://stackoverflow.com/questions/42022950/which-seeds-have-to-be-set-where-to-realize-100-reproducibility-of-training-res


from keras import backend as K
tf.set_random_seed(1234)

#%%
numEpochs = 1
#%%
"""
geneExpFile="C:/Data/GE_mvg.csv"
annotationFile="C:/Data/Annotation.csv"
geneNamesFile="C:/Data/CV_genes.csv"
geneNamesDataframe = read_csv(geneNamesFile ,dtype={"gene": str})
geneNames =  list(geneNamesDataframe['gene'])
df = read_csv(geneExpFile, names = geneNames)# ,header = None)
dfStd = df.std()
dfMean = dfStd.mean()
df=df.loc[:, df.std() > (dfMean)]
ut.isNullData(df)
df.to_csv('./274_genes.csv')
"""
df = read_csv('./274_genes.csv')# ,header = None)

geneExpression = np.array(df)
geneExpression = geneExpression[:,1:]

#%%
scaler = MinMaxScaler()
scaler.fit(geneExpression)
X = scaler.transform(geneExpression)
#one-hot Encoding:
lbls = read_csv('./Cluster_Lables.csv')
lbls = np.array(lbls)
lbls = lbls[:,1:]
#%% VAIN Model
X_train, X_test, y_train, y_test = ut.train_test_split(X,lbls, 0.5)
singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data_self(X_train , y_train)
singleton_test , pairwise_test , labels_test ,pairwiseLabels_test = ut.get_data_self(X_test , y_test)

import ThesisModel_AttentionMatrix as tm
import keras
#import ThesisModel as t
VAIN_Clustering  = tm.get_full_model(singleton_train.shape[1],singleton_train.shape[0],lbls.shape[1])
epochcount = 1
adam = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0)#, amsgrad=False)
VAIN_Clustering.compile(loss='categorical_crossentropy', optimizer= adam, metrics=['accuracy'])

try:
    while True:
        VAIN_Clustering_History = VAIN_Clustering.fit([singleton_train , pairwise_train], labels_train, batch_size = singleton_train.shape[0],epochs=epochcount ,validation_split = 0.2, verbose=2)# , callbacks =[tensorboard])# , trace.attention()])
        print("Epoch number %d" %epochcount)
        epochcount= epochcount+1
except KeyboardInterrupt:
    pass
testAcc_VAIN_Clustering = VAIN_Clustering.evaluate([singleton_test,pairwise_test],labels_test)



print("VAIN Model")
print(VAIN_Clustering.metrics_names)
print(testAcc_VAIN_Clustering)
ut.plotHistory(VAIN_Clustering_History ,"VIAN with clusters")
ut.saveModel(VAIN_Clustering , "VAIN_Clustering_noindex"+str(numEpochs))
"""
attention_mat = ut.getAttentionForSingleAgent(singleton_train[0] , pairwise_train[0],VAIN_Clustering)#cellPCA_model)
#attention_mat = attention_mat.reshape(499,499)
print(attention_mat)
"""


fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(8, 8))
"""
df['A'].plot(ax=axes[0,0]); axes[0,0].set_title('A')

df['B'].plot(ax=axes[0,1]); axes[0,1].set_title('B')

df['C'].plot(ax=axes[1,0]); axes[1,0].set_title('C')

df['D'].plot(ax=axes[1,1]); axes[1,1].set_title('D')

attention=[]
attention.append([])
attention.append([])
attention.append([])
attention.append([])
attention.append([])

for col_indx in range(0, labels_train.shape[1]):
     #make mask from clusterLabels
     hist = [0,0,0,0,0]
     all_col = labels_train[:,col_indx]
     cluster_mask = all_col > 0
     singletonFeaturesForClusterX = singleton_train[cluster_mask]
     pairwiseFeaturesForClusterX = pairwise_train[cluster_mask]
     LablesforOtherAgents = pairwiseLabels_train[cluster_mask]
     histForClusterX = [0,0,0,0,0]
     index = 0
     for classMember in singletonFeaturesForClusterX:
         singleton = classMember
         pairwise = pairwiseFeaturesForClusterX[index]
         othersLabels = LablesforOtherAgents[index]
         attention_vector = ut.getAttentionForSingleAgent(singleton , pairwise,VAIN_Clustering)#cellPCA_model)
         attention[col_indx].append(attention_vector)
         histForClusterX += ut.getHistForAttentionVector(attention_vector , othersLabels)
         index+=1
    #plot the graph for this agent
    #x-axis all agents lables
    #y-axis the count of the number of times they were given attention
     indx0 = int(col_indx/2)
     indx1 = int(col_indx%2)

     pd.DataFrame(histForClusterX, columns=['attention count']).plot(ax=axes[indx0,indx1],kind='bar',title='Attention Mechanism '+' for cluster'+str(col_indx))
#%%
ut.saveModel(VAIN_Clustering , "VAIN_Clustering"+str(numEpochs))

"""
