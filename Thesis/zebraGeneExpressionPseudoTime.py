#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 15:05:32 2018

@author: a
"""
#get the data
#split up the data
#get the model
# run the model
import utils as ut
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from pandas import read_csv
from sklearn.preprocessing import StandardScaler , MinMaxScaler
from sklearn.decomposition import PCA
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
import numpy as np
#%%
def getAttentionForAgent( singleton_sample, pairwise_sample,model):
    singleton_sample = np.expand_dims((singleton_sample), axis=0)
    pairwise_sample = np.expand_dims((pairwise_sample), axis=0)
    attention_vector = au.get_activations_simple(model,singleton_sample,pairwise_sample, print_shape_only=True,layer_name='attention_vec')
    attention_vector= attention_vector[0].flatten()
    return attention_vector
#ut.fixSeeds()
#%%
numEpochs = 1000
#%%
geneExpFile="C:/Data/GE_mvg.csv"
annotationFile="C:/Data/Annotation.csv"
df = read_csv(geneExpFile)# ,header = None)
geneExpression = np.array(df)
Annotations= read_csv(annotationFile)
pseudoTime = Annotations['Pseudotime']
#%%
scaler = MinMaxScaler()
scaler.fit(geneExpression)
X = scaler.transform(geneExpression)
dims = 5
pca = PCA(n_components=dims)
#fit PCA model to data
pca.fit(X)
X = pca.transform(X)
"""
pca2d = PCA(n_components=2)
#fit PCA model to data
pca2d.fit(X)
X2d = pca2d.transform(X)
ut.Kmeans(X2d,5)
"""
#%%
labels = np.array(pseudoTime).reshape(-1 , 1)

#%%Split Data
X_train, X_test, y_train, y_test = ut.train_test_split(X,labels, 0.5)
#%% 2dInput
singleton_train , pairwise_train , labels_train , pairwiseLabels_train = ut.get_data(X_train , y_train)
singleton_test , pairwise_test , labels_test ,pairwiseLabels_test = ut.get_data(X_test , y_test)
#%% Main Part :model trained on clustering labels
import ThesisModel as tm
model = tm.get_full_model(singleton_train.shape[1],singleton_train.shape[0],labels.shape[1])
model , interactionHistory = ut.compileRegression(model, singleton_train, pairwise_train, labels_train,2,numEpochs)
testAcc = model.evaluate([singleton_test,pairwise_test],labels_test)

ut.plotHistoryloss(interactionHistory, "pseudo-time interaction")
print("pseudo-time interaction")
print(model.metrics_names)
print(testAcc)
#%% Model trained on cell PCA
import ThesisModel as tm
cellPCA_model = tm.get_full_model(singleton_train.shape[1],singleton_train.shape[0],singleton_train.shape[1])
cellPCA_model , cellPCA_modelHistory = ut.compile_fit_TwoInputModel(cellPCA_model, singleton_train, pairwise_train, singleton_train,2,numEpochs)
cellPCA_model_testAcc = cellPCA_model.evaluate([singleton_test,pairwise_test],singleton_test)

ut.plotHistory(cellPCA_modelHistory, "cellPCA_model")
print("cellPCA_model Attention Model")
print(cellPCA_model.metrics_names)
print(cellPCA_model_testAcc)


#%%model trained on Gene PCA
#ut.plotModel(model,'model_full__attention.png')
#%% Print attention
import attention_utils as au
print (pairwise_train[0])
#attention_vector = au.get_activations(model,(singleton_train[0],pairwise_train[0]), print_shape_only=True,layer_name='attention_vec')
#[0].flatten()
print((singleton_train[0]).shape)
print((pairwise_train[0]).shape)

attention_vector = getAttentionForAgent(singleton_train[0] , pairwise_train[0] )

print('attention =', attention_vector)
#%%
# plot part.
import matplotlib.pyplot as plt
import pandas as pd

pd.DataFrame(attention_vector, columns=['attention (%)']).plot(kind='bar',
                                                               title='Attention Mechanism as '
                                                                     'a function of input'
                                                                     ' dimensions.')
plt.show()

#%%
def getHistForAttentionVector(attention_vector, labels_of_other_agents):
    mean = np.mean(attention_vector)
    mask = attention_vector>mean
    labels_of_other_agents = labels_of_other_agents[mask]
    histogram_other_labels = np.sum(labels_of_other_agents, axis = 0)
    return histogram_other_labels

#%%
#Goal : for every agent type Which other types get the most attention ?
# output : graph attention assigned to other classes for every given input class
#get all data points with cluster labels x
fig, axes = plt.subplots(nrows=3, ncols=2, figsize=(8, 8))
"""
df['A'].plot(ax=axes[0,0]); axes[0,0].set_title('A')

df['B'].plot(ax=axes[0,1]); axes[0,1].set_title('B')

df['C'].plot(ax=axes[1,0]); axes[1,0].set_title('C')

df['D'].plot(ax=axes[1,1]); axes[1,1].set_title('D')
"""

for col_indx in range(0, labels_train.shape[1]):
     #make mask from clusterLabels
     hist = [0,0,0,0] #,0]
     all_col = labels_train[:,col_indx]
     cluster_mask = all_col > 0
     singletonFeaturesForClusterX = singleton_train[cluster_mask]
     pairwiseFeaturesForClusterX = pairwise_train[cluster_mask]
     LablesforOtherAgents = pairwiseLabels_train[cluster_mask]
     histForClusterX = [0,0,0,0] #,0]
     index = 0
     for classMember in singletonFeaturesForClusterX:
         singleton = classMember
         pairwise = pairwiseFeaturesForClusterX[index]
         othersLabels = LablesforOtherAgents[index]
         attention_vector = getAttentionForAgent(singleton , pairwise,model_rigged)#cellPCA_model)
         histForClusterX += getHistForAttentionVector(attention_vector , othersLabels)
         index+=1
    #plot the graph for this agent
    #x-axis all agents lables
    #y-axis the count of the number of times they were given attention
     indx0 = int(col_indx/2)
     indx1 = int(col_indx%2)

     pd.DataFrame(histForClusterX, columns=['attention count']).plot(ax=axes[indx0,indx1],kind='bar',title='Attention Mechanism '+' for cluster'+str(col_indx))
#%%
ut.saveModel(cellPCA_model , "cellPCA_model_10000")
